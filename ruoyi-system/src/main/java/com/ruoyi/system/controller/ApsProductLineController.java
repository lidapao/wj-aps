package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.vo.ApsLineProcessVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsProductLine;
import com.ruoyi.system.service.IApsProductLineService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产线管理Controller
 *
 * @author ruoyi
 * @date 2024-01-06
 */
@Api(tags = "产线管理")
@RestController
@RequestMapping("/system/productLine")
public class ApsProductLineController extends BaseController
{
    @Autowired
    private IApsProductLineService apsProductLineService;

    /**
     * 查询产线管理列表
     */
    @ApiOperation("查询产线管理列表")
    @PreAuthorize("@ss.hasPermi('system:productLine:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApsProductLine apsProductLine)
    {
        startPage();
        List<ApsProductLine> list = apsProductLineService.selectApsProductLineList(apsProductLine);
        return getDataTable(list);
    }

    /**
     * 导出产线管理列表
     */
    @ApiOperation("导出产线管理列表")
    @PreAuthorize("@ss.hasPermi('system:productLine:export')")
    @Log(title = "产线管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsProductLine apsProductLine)
    {
        List<ApsProductLine> list = apsProductLineService.selectApsProductLineList(apsProductLine);
        ExcelUtil<ApsProductLine> util = new ExcelUtil<ApsProductLine>(ApsProductLine.class);
        util.exportExcel(response, list, "产线管理数据");
    }

    /**
     * 获取产线管理详细信息
     */
    @ApiOperation("获取产线管理详细信息")
    @PreAuthorize("@ss.hasPermi('system:productLine:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apsProductLineService.selectApsProductLineById(id));
    }

    /**
     * 新增产线管理
     */
    @ApiOperation("新增产线管理")
    @PreAuthorize("@ss.hasPermi('system:productLine:add')")
    @Log(title = "产线管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsProductLine apsProductLine)
    {
        return toAjax(apsProductLineService.insertApsProductLine(apsProductLine));
    }

    /**
     * 产线与工序绑定功能
     */
    @ApiOperation("产线与工序绑定功能")
    @PreAuthorize("@ss.hasPermi('system:productLine:add')")
    @Log(title = "产线管理", businessType = BusinessType.INSERT)
    @PostMapping("/bind")
    public AjaxResult bind(@RequestBody ApsLineProcessVo apsLineProcessVo)
    {
        return toAjax(apsProductLineService.bindLineAndProcess(apsLineProcessVo));
    }

    /**
     * 修改产线管理
     */
    @ApiOperation("修改产线管理")
    @PreAuthorize("@ss.hasPermi('system:productLine:edit')")
    @Log(title = "产线管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsProductLine apsProductLine)
    {
        return toAjax(apsProductLineService.updateApsProductLine(apsProductLine));
    }

    /**
     * 删除产线管理
     */
    @ApiOperation("删除产线管理")
    @PreAuthorize("@ss.hasPermi('system:productLine:remove')")
    @Log(title = "产线管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apsProductLineService.deleteApsProductLineByIds(ids));
    }
}
