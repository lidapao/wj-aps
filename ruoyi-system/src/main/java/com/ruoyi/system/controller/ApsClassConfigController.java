package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsClassConfig;
import com.ruoyi.system.service.IApsClassConfigService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 排班规则Controller
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Api(tags = "排班规则")
@RestController
@RequestMapping("/system/classConfig")
public class ApsClassConfigController extends BaseController
{
    @Autowired
    private IApsClassConfigService apsClassConfigService;

    /**
     * 查询排班规则列表
     */
    @ApiOperation("查询排班规则列表")
    @PreAuthorize("@ss.hasPermi('system:classConfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApsClassConfig apsClassConfig)
    {
        startPage();
        List<ApsClassConfig> list = apsClassConfigService.selectApsClassConfigList(apsClassConfig);
        return getDataTable(list);
    }

    /**
     * 导出排班规则列表
     */
    @ApiOperation("导出排班规则列表")
    @PreAuthorize("@ss.hasPermi('system:classConfig:export')")
    @Log(title = "排班规则", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsClassConfig apsClassConfig)
    {
        List<ApsClassConfig> list = apsClassConfigService.selectApsClassConfigList(apsClassConfig);
        ExcelUtil<ApsClassConfig> util = new ExcelUtil<ApsClassConfig>(ApsClassConfig.class);
        util.exportExcel(response, list, "排班规则数据");
    }

    /**
     * 获取排班规则详细信息
     */
    @ApiOperation("获取排班规则详细信息")
    @PreAuthorize("@ss.hasPermi('system:classConfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apsClassConfigService.selectApsClassConfigById(id));
    }

    /**
     * 新增排班规则
     */
    @ApiOperation("新增排班规则")
    @PreAuthorize("@ss.hasPermi('system:classConfig:add')")
    @Log(title = "排班规则", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsClassConfig apsClassConfig)
    {
        return toAjax(apsClassConfigService.insertApsClassConfig(apsClassConfig));
    }

    /**
     * 修改排班规则
     */
    @ApiOperation("修改排班规则")
    @PreAuthorize("@ss.hasPermi('system:classConfig:edit')")
    @Log(title = "排班规则", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsClassConfig apsClassConfig)
    {
        return toAjax(apsClassConfigService.updateApsClassConfig(apsClassConfig));
    }

    /**
     * 删除排班规则
     */
    @ApiOperation("删除排班规则")
    @PreAuthorize("@ss.hasPermi('system:classConfig:remove')")
    @Log(title = "排班规则", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apsClassConfigService.deleteApsClassConfigByIds(ids));
    }
}
