package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsEveryWorkDay;
import com.ruoyi.system.service.IApsEveryWorkDayService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 每日日历Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Api(tags = "每日日历")
@RestController
@RequestMapping("/system/everyWorkDay")
public class ApsEveryWorkDayController extends BaseController
{
    @Autowired
    private IApsEveryWorkDayService apsEveryWorkDayService;

    /**
     * 查询每日日历列表
     */
    @ApiOperation("查询每日日历列表")
    @PreAuthorize("@ss.hasPermi('system:everyWorkDay:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApsEveryWorkDay apsEveryWorkDay)
    {
        startPage();
        List<ApsEveryWorkDay> list = apsEveryWorkDayService.selectApsEveryWorkDayList(apsEveryWorkDay);
        return getDataTable(list);
    }

    /**
     * 导出每日日历列表
     */
    @ApiOperation("导出每日日历列表")
    @PreAuthorize("@ss.hasPermi('system:everyWorkDay:export')")
    @Log(title = "每日日历", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsEveryWorkDay apsEveryWorkDay)
    {
        List<ApsEveryWorkDay> list = apsEveryWorkDayService.selectApsEveryWorkDayList(apsEveryWorkDay);
        ExcelUtil<ApsEveryWorkDay> util = new ExcelUtil<ApsEveryWorkDay>(ApsEveryWorkDay.class);
        util.exportExcel(response, list, "每日日历数据");
    }

    /**
     * 获取每日日历详细信息
     */
    @ApiOperation("获取每日日历详细信息")
    @PreAuthorize("@ss.hasPermi('system:everyWorkDay:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apsEveryWorkDayService.selectApsEveryWorkDayById(id));
    }

    /**
     * 新增每日日历
     */
    @ApiOperation("新增每日日历")
    @PreAuthorize("@ss.hasPermi('system:everyWorkDay:add')")
    @Log(title = "每日日历", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsEveryWorkDay apsEveryWorkDay)
    {
        return toAjax(apsEveryWorkDayService.insertApsEveryWorkDay(apsEveryWorkDay));
    }

    /**
     * 修改每日日历
     */
    @ApiOperation("修改每日日历")
    @PreAuthorize("@ss.hasPermi('system:everyWorkDay:edit')")
    @Log(title = "每日日历", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsEveryWorkDay apsEveryWorkDay)
    {
        return toAjax(apsEveryWorkDayService.updateApsEveryWorkDay(apsEveryWorkDay));
    }

    // /**
    //  * 删除每日日历
    //  */
    // @PreAuthorize("@ss.hasPermi('system:everyWorkDay:remove')")
    // @Log(title = "每日日历", businessType = BusinessType.DELETE)
	// @DeleteMapping("/{ids}")
    // public AjaxResult remove(@PathVariable Long[] ids)
    // {
    //     return toAjax(apsEveryWorkDayService.deleteApsEveryWorkDayByIds(ids));
    // }

    /**
     * 删除每日日历
     */
    @ApiOperation("删除每日日历")
    @PreAuthorize("@ss.hasPermi('system:everyWorkDay:remove')")
    @Log(title = "每日日历", businessType = BusinessType.DELETE)
    @DeleteMapping
    public AjaxResult remove(@RequestBody ApsEveryWorkDay apsEveryWorkDay)
    {
        return toAjax(apsEveryWorkDayService.deleteApsEveryWorkDay(apsEveryWorkDay));
    }
}
