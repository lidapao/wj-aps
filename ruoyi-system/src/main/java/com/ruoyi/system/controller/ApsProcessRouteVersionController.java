package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsProcessRouteVersion;
import com.ruoyi.system.service.IApsProcessRouteVersionService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工艺路线版本Controller
 *
 * @author ruoyi
 * @date 2024-01-09
 */
@Api(tags = "工艺路线版本")
@RestController
@RequestMapping("/system/processRouteVersion")
public class ApsProcessRouteVersionController extends BaseController
{
    @Autowired
    private IApsProcessRouteVersionService apsProcessRouteVersionService;

    /**
     * 查询工艺路线版本列表
     */
    @ApiOperation("查询工艺路线版本列表")
    @PreAuthorize("@ss.hasPermi('system:processRouteVersion:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApsProcessRouteVersion apsProcessRouteVersion)
    {
        startPage();
        List<ApsProcessRouteVersion> list = apsProcessRouteVersionService.selectApsProcessRouteVersionList(apsProcessRouteVersion);
        return getDataTable(list);
    }

    /**
     * 导出工艺路线版本列表
     */
    @ApiOperation("导出工艺路线版本列表")
    @PreAuthorize("@ss.hasPermi('system:processRouteVersion:export')")
    @Log(title = "工艺路线版本", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsProcessRouteVersion apsProcessRouteVersion)
    {
        List<ApsProcessRouteVersion> list = apsProcessRouteVersionService.selectApsProcessRouteVersionList(apsProcessRouteVersion);
        ExcelUtil<ApsProcessRouteVersion> util = new ExcelUtil<ApsProcessRouteVersion>(ApsProcessRouteVersion.class);
        util.exportExcel(response, list, "工艺路线版本数据");
    }

    /**
     * 获取工艺路线版本详细信息
     */
    @ApiOperation("获取工艺路线版本详细信息")
    @PreAuthorize("@ss.hasPermi('system:processRouteVersion:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apsProcessRouteVersionService.selectApsProcessRouteVersionById(id));
    }

    /**
     * 获取指定工艺路线版本列表
     */
    @ApiOperation("获取指定工艺路线版本列表")
    @PreAuthorize("@ss.hasPermi('system:processRouteVersion:query')")
    @GetMapping(value = "/listByCode/{code}")
    public TableDataInfo listByCode(@PathVariable("code") String code)
    {
        startPage();
        List<ApsProcessRouteVersion> list = apsProcessRouteVersionService.selectApsProcessRouteVersionByCode(code);
        return getDataTable(list);
    }

    /**
     * 新增工艺路线版本
     */
    @ApiOperation("新增工艺路线版本")
    @PreAuthorize("@ss.hasPermi('system:processRouteVersion:add')")
    @Log(title = "工艺路线版本", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsProcessRouteVersion apsProcessRouteVersion)
    {
        return toAjax(apsProcessRouteVersionService.insertApsProcessRouteVersion(apsProcessRouteVersion));
    }

    /**
     * 修改工艺路线版本
     */
    @ApiOperation("修改工艺路线版本")
    @PreAuthorize("@ss.hasPermi('system:processRouteVersion:edit')")
    @Log(title = "工艺路线版本", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsProcessRouteVersion apsProcessRouteVersion)
    {
        return toAjax(apsProcessRouteVersionService.updateApsProcessRouteVersion(apsProcessRouteVersion));
    }

    /**
     * 删除工艺路线版本
     */
    @ApiOperation("删除工艺路线版本")
    @PreAuthorize("@ss.hasPermi('system:processRouteVersion:remove')")
    @Log(title = "工艺路线版本", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apsProcessRouteVersionService.deleteApsProcessRouteVersionByIds(ids));
    }
}
