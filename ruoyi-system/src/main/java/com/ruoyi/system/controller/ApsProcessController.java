package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.vo.ApsProcessVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsProcess;
import com.ruoyi.system.service.IApsProcessService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产品工序Controller
 *
 * @author ruoyi
 * @date 2024-01-06
 */
@Api(tags = "产品工序")
@RestController
@RequestMapping("/system/process")
public class ApsProcessController extends BaseController
{
    @Autowired
    private IApsProcessService apsProcessService;

    /**
     * 查询产品工序列表
     */
    @ApiOperation("查询产品工序列表")
    @PreAuthorize("@ss.hasPermi('system:process:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApsProcess apsProcess)
    {
        startPage();
        List<ApsProcess> list = apsProcessService.selectApsProcessList(apsProcess);
        return getDataTable(list);
    }

    /**
     * 查询指定工艺路线版本下的工序列表
     */
    @ApiOperation("查询指定工艺路线版本下的工序列表")
    @PreAuthorize("@ss.hasPermi('system:process:list')")
    @GetMapping("/listByVersionId/{versionId}")
    public TableDataInfo listByVersionId(@PathVariable("versionId") Long versionId)
    {
        startPage();
        List<ApsProcess> list = apsProcessService.selectApsProcessListByVersionId(versionId);
        return getDataTable(list);
    }

    /**
     * 新增指定工艺路线版本下产品工序
     */
    @ApiOperation("新增指定工艺路线版本下产品工序")
    @PreAuthorize("@ss.hasPermi('system:process:add')")
    @Log(title = "产品工序", businessType = BusinessType.INSERT)
    @PostMapping("/bindProcess")
    public AjaxResult bindProcess(@RequestBody ApsProcessVo apsProcessVo)
    {
        // todo 增加绑定关系 和 查询 工序是否存在
        return toAjax(apsProcessService.bindProcess(apsProcessVo));
    }

    /**
     * 导出产品工序列表
     */
    @ApiOperation("导出产品工序列表")
    @PreAuthorize("@ss.hasPermi('system:process:export')")
    @Log(title = "产品工序", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsProcess apsProcess)
    {
        List<ApsProcess> list = apsProcessService.selectApsProcessList(apsProcess);
        ExcelUtil<ApsProcess> util = new ExcelUtil<ApsProcess>(ApsProcess.class);
        util.exportExcel(response, list, "产品工序数据");
    }

    /**
     * 获取产品工序详细信息
     */
    @ApiOperation("获取产品工序详细信息")
    @PreAuthorize("@ss.hasPermi('system:process:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apsProcessService.selectApsProcessById(id));
    }

    /**
     * 新增产品工序
     */
    @ApiOperation("新增产品工序")
    @PreAuthorize("@ss.hasPermi('system:process:add')")
    @Log(title = "产品工序", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsProcess apsProcess)
    {
        return toAjax(apsProcessService.insertApsProcess(apsProcess));
    }

    /**
     * 修改产品工序
     */
    @ApiOperation("修改产品工序")
    @PreAuthorize("@ss.hasPermi('system:process:edit')")
    @Log(title = "产品工序", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsProcess apsProcess)
    {
        return toAjax(apsProcessService.updateApsProcess(apsProcess));
    }

    /**
     * 删除产品工序
     */
    @ApiOperation("删除产品工序")
    @PreAuthorize("@ss.hasPermi('system:process:remove')")
    @Log(title = "产品工序", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apsProcessService.deleteApsProcessByIds(ids));
    }
}
