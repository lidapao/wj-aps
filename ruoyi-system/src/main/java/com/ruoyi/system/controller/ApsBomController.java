package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsBom;
import com.ruoyi.system.service.IApsBomService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * BOM管理Controller
 *
 * @author ruoyi
 * @date 2024-01-09
 */
@Api(tags = "BOM管理")
@RestController
@RequestMapping("/system/bom")
public class ApsBomController extends BaseController
{
    @Autowired
    private IApsBomService apsBomService;

    // /**
    //  * 查询BOM管理列表
    //  */
    // @PreAuthorize("@ss.hasPermi('system:bom:list')")
    // @GetMapping("/list")
    // public TableDataInfo list(ApsBom apsBom)
    // {
    //     startPage();
    //     List<ApsBom> list = apsBomService.selectApsBomList(apsBom);
    //     return getDataTable(list);
    // }

    // /**
    //  * 查询物料信息列表 - 层级关系
    //  */
    // @PreAuthorize("@ss.hasPermi('system:bom:list')")
    // @GetMapping("/list")
    // public TableDataInfo listTree(SysBom sysBom)
    // {
    //     List<SysBom> sysBoms = sysBomService.selectSysBomList(sysBom);
    //     List<SysBom> bomPageList = sysBomService.selectSysBomPageList(sysBom);
    //     List<SysBom> list = sysBomService.selectSysBomTreeList(bomPageList, sysBoms);
    //     return getDataTable(list);
    // }

    /**
     * 查询BOM管理列表 - 层级关系 - 去重
     */
    @ApiOperation("查询BOM管理列表 - 层级关系 - 去重")
    @PreAuthorize("@ss.hasPermi('system:bom:listUnique')")
    @GetMapping("/listUnique")
    public TableDataInfo listTreeUnique(ApsBom sysBom)
    {
        ApsBom bom = new ApsBom();
        List<ApsBom> sysBoms = apsBomService.selectSysBomListUnique(bom);
        List<ApsBom> bomPageList = apsBomService.selectSysBomPageList(sysBom);
        List<ApsBom> list = apsBomService.selectSysBomTreeList(bomPageList, sysBoms);
        return getDataTable(list);
    }

    /**
     * 导出BOM管理列表
     */
    @ApiOperation("导出BOM管理列表")
    @PreAuthorize("@ss.hasPermi('system:bom:export')")
    @Log(title = "BOM管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsBom apsBom)
    {
        List<ApsBom> list = apsBomService.selectApsBomList(apsBom);
        ExcelUtil<ApsBom> util = new ExcelUtil<ApsBom>(ApsBom.class);
        util.exportExcel(response, list, "BOM管理数据");
    }

    /**
     * 获取BOM管理详细信息
     */
    @ApiOperation("获取BOM管理详细信息")
    @PreAuthorize("@ss.hasPermi('system:bom:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apsBomService.selectApsBomById(id));
    }

    /**
     * 新增BOM管理
     */
    @ApiOperation("新增BOM管理")
    @PreAuthorize("@ss.hasPermi('system:bom:add')")
    @Log(title = "BOM管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsBom apsBom)
    {
        return toAjax(apsBomService.insertApsBom(apsBom));
    }

    /**
     * 修改BOM管理
     */
    @ApiOperation("修改BOM管理")
    @PreAuthorize("@ss.hasPermi('system:bom:edit')")
    @Log(title = "BOM管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsBom apsBom)
    {
        return toAjax(apsBomService.updateApsBom(apsBom));
    }

    /**
     * 删除BOM管理
     */
    @ApiOperation("删除BOM管理")
    @PreAuthorize("@ss.hasPermi('system:bom:remove')")
    @Log(title = "BOM管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apsBomService.deleteApsBomByIds(ids));
    }
}
