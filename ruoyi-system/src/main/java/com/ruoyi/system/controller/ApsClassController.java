package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsClass;
import com.ruoyi.system.service.IApsClassService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 班次管理Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Api(tags = "班次管理")
@RestController
@RequestMapping("/system/class")
public class ApsClassController extends BaseController
{
    @Autowired
    private IApsClassService apsClassService;

    /**
     * 查询班次管理列表
     */
    @ApiOperation("查询班次管理列表")
    @PreAuthorize("@ss.hasPermi('system:class:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApsClass apsClass)
    {
        startPage();
        List<ApsClass> list = apsClassService.selectApsClassList(apsClass);
        return getDataTable(list);
    }

    /**
     * 导出班次管理列表
     */
    @ApiOperation("导出班次管理列表")
    @PreAuthorize("@ss.hasPermi('system:class:export')")
    @Log(title = "班次管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsClass apsClass)
    {
        List<ApsClass> list = apsClassService.selectApsClassList(apsClass);
        ExcelUtil<ApsClass> util = new ExcelUtil<ApsClass>(ApsClass.class);
        util.exportExcel(response, list, "班次管理数据");
    }

    /**
     * 获取班次管理详细信息
     */
    @ApiOperation("获取班次管理详细信息")
    @PreAuthorize("@ss.hasPermi('system:class:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apsClassService.selectApsClassById(id));
    }

    /**
     * 新增班次管理
     */
    @ApiOperation("新增班次管理")
    @PreAuthorize("@ss.hasPermi('system:class:add')")
    @Log(title = "班次管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsClass apsClass)
    {
        return toAjax(apsClassService.insertApsClass(apsClass));
    }

    /**
     * 修改班次管理
     */
    @ApiOperation("修改班次管理")
    @PreAuthorize("@ss.hasPermi('system:class:edit')")
    @Log(title = "班次管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsClass apsClass)
    {
        return toAjax(apsClassService.updateApsClass(apsClass));
    }

    /**
     * 删除班次管理
     */
    @ApiOperation("删除班次管理")
    @PreAuthorize("@ss.hasPermi('system:class:remove')")
    @Log(title = "班次管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apsClassService.deleteApsClassByIds(ids));
    }
}
