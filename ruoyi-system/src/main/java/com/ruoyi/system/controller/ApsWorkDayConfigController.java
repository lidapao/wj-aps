package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsWorkDayConfig;
import com.ruoyi.system.service.IApsWorkDayConfigService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工作日历设置规则Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Api(tags = "工作日历设置规则")
@RestController
@RequestMapping("/system/workDayConfig")
public class ApsWorkDayConfigController extends BaseController
{
    @Autowired
    private IApsWorkDayConfigService apsWorkDayConfigService;

    /**
     * 查询工作日历设置规则列表
     */
    @ApiOperation("查询工作日历设置规则列表")
    @PreAuthorize("@ss.hasPermi('system:workDayConfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApsWorkDayConfig apsWorkDayConfig)
    {
        startPage();
        List<ApsWorkDayConfig> list = apsWorkDayConfigService.selectApsWorkDayConfigList(apsWorkDayConfig);
        return getDataTable(list);
    }

    /**
     * 导出工作日历设置规则列 表
     */
    @ApiOperation("导出工作日历设置规则列表")
    @PreAuthorize("@ss.hasPermi('system:workDayConfig:export')")
    @Log(title = "工作日历设置规则", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsWorkDayConfig apsWorkDayConfig)
    {
        List<ApsWorkDayConfig> list = apsWorkDayConfigService.selectApsWorkDayConfigList(apsWorkDayConfig);
        ExcelUtil<ApsWorkDayConfig> util = new ExcelUtil<ApsWorkDayConfig>(ApsWorkDayConfig.class);
        util.exportExcel(response, list, "工作日历设置规则数据");
    }

    /**
     * 获取工作日历设置规则详细信息
     */
    @ApiOperation("获取工作日历设置规则详细信息")
    @PreAuthorize("@ss.hasPermi('system:workDayConfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apsWorkDayConfigService.selectApsWorkDayConfigById(id));
    }

    /**
     * 新增工作日历设置规则
     */
    @ApiOperation("新增工作日历设置规则")
    @PreAuthorize("@ss.hasPermi('system:workDayConfig:add')")
    @Log(title = "工作日历设置规则", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsWorkDayConfig apsWorkDayConfig)
    {
        return toAjax(apsWorkDayConfigService.insertApsWorkDayConfig(apsWorkDayConfig));
    }

    /**
     * 修改工作日历设置规则
     */
    @ApiOperation("修改工作日历设置规则")
    @PreAuthorize("@ss.hasPermi('system:workDayConfig:edit')")
    @Log(title = "工作日历设置规则", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsWorkDayConfig apsWorkDayConfig)
    {
        return toAjax(apsWorkDayConfigService.updateApsWorkDayConfig(apsWorkDayConfig));
    }

    /**
     * 删除工作日历设置规则
     */
    @ApiOperation("删除工作日历设置规则")
    @PreAuthorize("@ss.hasPermi('system:workDayConfig:remove')")
    @Log(title = "工作日历设置规则", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apsWorkDayConfigService.deleteApsWorkDayConfigByIds(ids));
    }
}
