package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsVersionProcess;
import com.ruoyi.system.service.IApsVersionProcessService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工艺路线版本与工序关联表Controller
 *
 * @author ruoyi
 * @date 2024-01-10
 */
@Api(tags = "工艺路线版本与工序关联表")
@RestController
@RequestMapping("/system/versionProcess")
public class ApsVersionProcessController extends BaseController
{
    @Autowired
    private IApsVersionProcessService apsVersionProcessService;

    /**
     * 查询工艺路线版本与工序关联表列表
     */
    @ApiOperation("查询工艺路线版本与工序关联表列表")
    @PreAuthorize("@ss.hasPermi('system:versionProcess:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApsVersionProcess apsVersionProcess)
    {
        startPage();
        List<ApsVersionProcess> list = apsVersionProcessService.selectApsVersionProcessList(apsVersionProcess);
        return getDataTable(list);
    }

    /**
     * 导出工艺路线版本与工序关联表列表
     */
    @ApiOperation("导出工艺路线版本与工序关联表列表")
    @PreAuthorize("@ss.hasPermi('system:versionProcess:export')")
    @Log(title = "工艺路线版本与工序关联表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsVersionProcess apsVersionProcess)
    {
        List<ApsVersionProcess> list = apsVersionProcessService.selectApsVersionProcessList(apsVersionProcess);
        ExcelUtil<ApsVersionProcess> util = new ExcelUtil<ApsVersionProcess>(ApsVersionProcess.class);
        util.exportExcel(response, list, "工艺路线版本与工序关联表数据");
    }

    /**
     * 获取工艺路线版本与工序关联表详细信息
     */
    @ApiOperation("获取工艺路线版本与工序关联表详细信息")
    @PreAuthorize("@ss.hasPermi('system:versionProcess:query')")
    @GetMapping(value = "/{versionId}")
    public AjaxResult getInfo(@PathVariable("versionId") Long versionId)
    {
        return success(apsVersionProcessService.selectApsVersionProcessByVersionId(versionId));
    }

    /**
     * 新增工艺路线版本与工序关联表
     */
    @ApiOperation("新增工艺路线版本与工序关联表")
    @PreAuthorize("@ss.hasPermi('system:versionProcess:add')")
    @Log(title = "工艺路线版本与工序关联表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsVersionProcess apsVersionProcess)
    {
        return toAjax(apsVersionProcessService.insertApsVersionProcess(apsVersionProcess));
    }

    /**
     * 修改工艺路线版本与工序关联表
     */
    @ApiOperation("修改工艺路线版本与工序关联表")
    @PreAuthorize("@ss.hasPermi('system:versionProcess:edit')")
    @Log(title = "工艺路线版本与工序关联表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsVersionProcess apsVersionProcess)
    {
        return toAjax(apsVersionProcessService.updateApsVersionProcess(apsVersionProcess));
    }

    /**
     * 删除工艺路线版本与工序关联表
     */
    @ApiOperation("删除工艺路线版本与工序关联表")
    @PreAuthorize("@ss.hasPermi('system:versionProcess:remove')")
    @Log(title = "工艺路线版本与工序关联表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{versionIds}")
    public AjaxResult remove(@PathVariable Long[] versionIds)
    {
        return toAjax(apsVersionProcessService.deleteApsVersionProcessByVersionIds(versionIds));
    }
}
