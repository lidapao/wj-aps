package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsLineProcess;
import com.ruoyi.system.service.IApsLineProcessService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产线工序关联表Controller
 *
 * @author ruoyi
 * @date 2024-01-06
 */
@Api(tags = "产线工序关联表")
@RestController
@RequestMapping("/system/lineProcess")
public class ApsLineProcessController extends BaseController
{
    @Autowired
    private IApsLineProcessService apsLineProcessService;

    /**
     * 查询产线工序关联表列表
     */
    @ApiOperation("查询产线工序关联表列表")
    @PreAuthorize("@ss.hasPermi('system:lineProcess:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApsLineProcess apsLineProcess)
    {
        startPage();
        List<ApsLineProcess> list = apsLineProcessService.selectApsLineProcessList(apsLineProcess);
        return getDataTable(list);
    }

    /**
     * 导出产线工序关联表列表
     */
    @ApiOperation("导出产线工序关联表列表")
    @PreAuthorize("@ss.hasPermi('system:lineProcess:export')")
    @Log(title = "产线工序关联表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsLineProcess apsLineProcess)
    {
        List<ApsLineProcess> list = apsLineProcessService.selectApsLineProcessList(apsLineProcess);
        ExcelUtil<ApsLineProcess> util = new ExcelUtil<ApsLineProcess>(ApsLineProcess.class);
        util.exportExcel(response, list, "产线工序关联表数据");
    }

    /**
     * 获取产线工序关联表详细信息
     */
    @ApiOperation("获取产线工序关联表详细信息")
    @PreAuthorize("@ss.hasPermi('system:lineProcess:query')")
    @GetMapping(value = "/{lineId}")
    public AjaxResult getInfo(@PathVariable("lineId") Long lineId)
    {
        return success(apsLineProcessService.selectApsLineProcessByLineId(lineId));
    }

    /**
     * 新增产线工序关联表
     */
    @ApiOperation("新增产线工序关联")
    @PreAuthorize("@ss.hasPermi('system:lineProcess:add')")
    @Log(title = "产线工序关联表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsLineProcess apsLineProcess)
    {
        return toAjax(apsLineProcessService.insertApsLineProcess(apsLineProcess));
    }

    /**
     * 修改产线工序关联表
     */
    @ApiOperation("修改产线工序关联")
    @PreAuthorize("@ss.hasPermi('system:lineProcess:edit')")
    @Log(title = "产线工序关联表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsLineProcess apsLineProcess)
    {
        return toAjax(apsLineProcessService.updateApsLineProcess(apsLineProcess));
    }

    /**
     * 删除产线工序关联表
     */
    @ApiOperation("删除产线工序关联")
    @PreAuthorize("@ss.hasPermi('system:lineProcess:remove')")
    @Log(title = "产线工序关联表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{lineIds}")
    public AjaxResult remove(@PathVariable Long[] lineIds)
    {
        return toAjax(apsLineProcessService.deleteApsLineProcessByLineIds(lineIds));
    }
}
