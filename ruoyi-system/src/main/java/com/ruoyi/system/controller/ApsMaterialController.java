package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ApsMaterial;
import com.ruoyi.system.service.IApsMaterialService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物料管理Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Api(tags = "物料管理")
@RestController
@RequestMapping("/system/material")
public class ApsMaterialController extends BaseController
{
    @Autowired
    private IApsMaterialService apsMaterialService;

    /**
     * 查询物料管理列表
     */
    @ApiOperation("查询物料管理列表")
    @PreAuthorize("@ss.hasPermi('system:material:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApsMaterial apsMaterial)
    {
        startPage();
        List<ApsMaterial> list = apsMaterialService.selectApsMaterialList(apsMaterial);
        return getDataTable(list);
    }

    /**
     * 导出物料管理列表
     */
    @ApiOperation("导出物料管理列表")
    @PreAuthorize("@ss.hasPermi('system:material:export')")
    @Log(title = "物料管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApsMaterial apsMaterial)
    {
        List<ApsMaterial> list = apsMaterialService.selectApsMaterialList(apsMaterial);
        ExcelUtil<ApsMaterial> util = new ExcelUtil<ApsMaterial>(ApsMaterial.class);
        util.exportExcel(response, list, "物料管理数据");
    }

    /**
     * 获取物料管理详细信息
     */
    @ApiOperation("获取物料管理详细信息")
    @PreAuthorize("@ss.hasPermi('system:material:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apsMaterialService.selectApsMaterialById(id));
    }

    /**
     * 新增物料管理
     */
    @ApiOperation("新增物料管理")
    @PreAuthorize("@ss.hasPermi('system:material:add')")
    @Log(title = "物料管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApsMaterial apsMaterial)
    {
        return toAjax(apsMaterialService.insertApsMaterial(apsMaterial));
    }

    /**
     * 修改物料管理
     */
    @ApiOperation("修改物料管理")
    @PreAuthorize("@ss.hasPermi('system:material:edit')")
    @Log(title = "物料管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApsMaterial apsMaterial)
    {
        return toAjax(apsMaterialService.updateApsMaterial(apsMaterial));
    }

    /**
     * 删除物料管理
     */
    @ApiOperation("删除物料管理")
    @PreAuthorize("@ss.hasPermi('system:material:remove')")
    @Log(title = "物料管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apsMaterialService.deleteApsMaterialByIds(ids));
    }
}
