package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ApsProductLine;

/**
 * 产线管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-06
 */
public interface ApsProductLineMapper 
{
    /**
     * 查询产线管理
     * 
     * @param id 产线管理主键
     * @return 产线管理
     */
    public ApsProductLine selectApsProductLineById(Long id);

    /**
     * 查询产线管理列表
     * 
     * @param apsProductLine 产线管理
     * @return 产线管理集合
     */
    public List<ApsProductLine> selectApsProductLineList(ApsProductLine apsProductLine);

    /**
     * 新增产线管理
     * 
     * @param apsProductLine 产线管理
     * @return 结果
     */
    public int insertApsProductLine(ApsProductLine apsProductLine);

    /**
     * 修改产线管理
     * 
     * @param apsProductLine 产线管理
     * @return 结果
     */
    public int updateApsProductLine(ApsProductLine apsProductLine);

    /**
     * 删除产线管理
     * 
     * @param id 产线管理主键
     * @return 结果
     */
    public int deleteApsProductLineById(Long id);

    /**
     * 批量删除产线管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteApsProductLineByIds(Long[] ids);
}
