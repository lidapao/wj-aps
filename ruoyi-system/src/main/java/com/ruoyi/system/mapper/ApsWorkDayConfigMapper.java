package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ApsWorkDayConfig;

/**
 * 工作日历设置规则Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ApsWorkDayConfigMapper 
{
    /**
     * 查询工作日历设置规则
     * 
     * @param id 工作日历设置规则主键
     * @return 工作日历设置规则
     */
    public ApsWorkDayConfig selectApsWorkDayConfigById(Long id);

    /**
     * 查询工作日历设置规则列表
     * 
     * @param apsWorkDayConfig 工作日历设置规则
     * @return 工作日历设置规则集合
     */
    public List<ApsWorkDayConfig> selectApsWorkDayConfigList(ApsWorkDayConfig apsWorkDayConfig);

    /**
     * 新增工作日历设置规则
     * 
     * @param apsWorkDayConfig 工作日历设置规则
     * @return 结果
     */
    public int insertApsWorkDayConfig(ApsWorkDayConfig apsWorkDayConfig);

    /**
     * 修改工作日历设置规则
     * 
     * @param apsWorkDayConfig 工作日历设置规则
     * @return 结果
     */
    public int updateApsWorkDayConfig(ApsWorkDayConfig apsWorkDayConfig);

    /**
     * 删除工作日历设置规则
     * 
     * @param id 工作日历设置规则主键
     * @return 结果
     */
    public int deleteApsWorkDayConfigById(Long id);

    /**
     * 批量删除工作日历设置规则
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteApsWorkDayConfigByIds(Long[] ids);
}
