package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ApsClass;

/**
 * 班次管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ApsClassMapper 
{
    /**
     * 查询班次管理
     * 
     * @param id 班次管理主键
     * @return 班次管理
     */
    public ApsClass selectApsClassById(Long id);

    /**
     * 查询班次管理列表
     * 
     * @param apsClass 班次管理
     * @return 班次管理集合
     */
    public List<ApsClass> selectApsClassList(ApsClass apsClass);

    /**
     * 新增班次管理
     * 
     * @param apsClass 班次管理
     * @return 结果
     */
    public int insertApsClass(ApsClass apsClass);

    /**
     * 修改班次管理
     * 
     * @param apsClass 班次管理
     * @return 结果
     */
    public int updateApsClass(ApsClass apsClass);

    /**
     * 删除班次管理
     * 
     * @param id 班次管理主键
     * @return 结果
     */
    public int deleteApsClassById(Long id);

    /**
     * 批量删除班次管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteApsClassByIds(Long[] ids);
}
