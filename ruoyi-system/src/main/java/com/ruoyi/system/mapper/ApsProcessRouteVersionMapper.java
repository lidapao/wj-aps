package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ApsProcessRouteVersion;

/**
 * 工艺路线版本Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-09
 */
public interface ApsProcessRouteVersionMapper
{
    /**
     * 查询工艺路线版本
     *
     * @param id 工艺路线版本主键
     * @return 工艺路线版本
     */
    public ApsProcessRouteVersion selectApsProcessRouteVersionById(Long id);

    public List<ApsProcessRouteVersion> selectApsProcessRouteVersionByCode(String code);

    /**
     * 查询工艺路线版本列表
     *
     * @param apsProcessRouteVersion 工艺路线版本
     * @return 工艺路线版本集合
     */
    public List<ApsProcessRouteVersion> selectApsProcessRouteVersionList(ApsProcessRouteVersion apsProcessRouteVersion);

    /**
     * 新增工艺路线版本
     *
     * @param apsProcessRouteVersion 工艺路线版本
     * @return 结果
     */
    public int insertApsProcessRouteVersion(ApsProcessRouteVersion apsProcessRouteVersion);

    /**
     * 修改工艺路线版本
     *
     * @param apsProcessRouteVersion 工艺路线版本
     * @return 结果
     */
    public int updateApsProcessRouteVersion(ApsProcessRouteVersion apsProcessRouteVersion);

    /**
     * 删除工艺路线版本
     *
     * @param id 工艺路线版本主键
     * @return 结果
     */
    public int deleteApsProcessRouteVersionById(Long id);

    /**
     * 批量删除工艺路线版本
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteApsProcessRouteVersionByIds(Long[] ids);
}
