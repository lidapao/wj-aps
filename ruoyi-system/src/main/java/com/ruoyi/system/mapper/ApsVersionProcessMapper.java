package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ApsVersionProcess;

/**
 * 工艺路线版本与工序关联表Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-10
 */
public interface ApsVersionProcessMapper
{
    /**
     * 查询工艺路线版本与工序关联表
     *
     * @param versionId 工艺路线版本与工序关联表主键
     * @return 工艺路线版本与工序关联表
     */
    public List<ApsVersionProcess> selectApsVersionProcessByVersionId(Long versionId);

    /**
     * 查询工艺路线版本与工序关联表列表
     *
     * @param apsVersionProcess 工艺路线版本与工序关联表
     * @return 工艺路线版本与工序关联表集合
     */
    public List<ApsVersionProcess> selectApsVersionProcessList(ApsVersionProcess apsVersionProcess);

    /**
     * 新增工艺路线版本与工序关联表
     *
     * @param apsVersionProcess 工艺路线版本与工序关联表
     * @return 结果
     */
    public int insertApsVersionProcess(ApsVersionProcess apsVersionProcess);

    /**
     * 修改工艺路线版本与工序关联表
     *
     * @param apsVersionProcess 工艺路线版本与工序关联表
     * @return 结果
     */
    public int updateApsVersionProcess(ApsVersionProcess apsVersionProcess);

    /**
     * 删除工艺路线版本与工序关联表
     *
     * @param versionId 工艺路线版本与工序关联表主键
     * @return 结果
     */
    public int deleteApsVersionProcessByVersionId(Long versionId);

    /**
     * 批量删除工艺路线版本与工序关联表
     *
     * @param versionIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteApsVersionProcessByVersionIds(Long[] versionIds);
}
