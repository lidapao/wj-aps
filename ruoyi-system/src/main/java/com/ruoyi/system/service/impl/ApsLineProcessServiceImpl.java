package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApsLineProcessMapper;
import com.ruoyi.system.domain.ApsLineProcess;
import com.ruoyi.system.service.IApsLineProcessService;

/**
 * 产线工序关联表Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-06
 */
@Service
public class ApsLineProcessServiceImpl implements IApsLineProcessService
{
    @Autowired
    private ApsLineProcessMapper apsLineProcessMapper;

    /**
     * 查询产线工序关联表
     *
     * @param lineId 产线工序关联表主键
     * @return 产线工序关联表
     */
    @Override
    public List<ApsLineProcess> selectApsLineProcessByLineId(Long lineId)
    {
        return apsLineProcessMapper.selectApsLineProcessByLineId(lineId);
    }

    /**
     * 查询产线工序关联表列表
     *
     * @param apsLineProcess 产线工序关联表
     * @return 产线工序关联表
     */
    @Override
    public List<ApsLineProcess> selectApsLineProcessList(ApsLineProcess apsLineProcess)
    {
        return apsLineProcessMapper.selectApsLineProcessList(apsLineProcess);
    }

    /**
     * 新增产线工序关联表
     *
     * @param apsLineProcess 产线工序关联表
     * @return 结果
     */
    @Override
    public int insertApsLineProcess(ApsLineProcess apsLineProcess)
    {
        return apsLineProcessMapper.insertApsLineProcess(apsLineProcess);
    }

    /**
     * 修改产线工序关联表
     *
     * @param apsLineProcess 产线工序关联表
     * @return 结果
     */
    @Override
    public int updateApsLineProcess(ApsLineProcess apsLineProcess)
    {
        return apsLineProcessMapper.updateApsLineProcess(apsLineProcess);
    }

    /**
     * 批量删除产线工序关联表
     *
     * @param lineIds 需要删除的产线工序关联表主键
     * @return 结果
     */
    @Override
    public int deleteApsLineProcessByLineIds(Long[] lineIds)
    {
        return apsLineProcessMapper.deleteApsLineProcessByLineIds(lineIds);
    }

    /**
     * 删除产线工序关联表信息
     *
     * @param lineId 产线工序关联表主键
     * @return 结果
     */
    @Override
    public int deleteApsLineProcessByLineId(Long lineId)
    {
        return apsLineProcessMapper.deleteApsLineProcessByLineId(lineId);
    }
}
