package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApsEveryWorkDayMapper;
import com.ruoyi.system.domain.ApsEveryWorkDay;
import com.ruoyi.system.service.IApsEveryWorkDayService;

/**
 * 每日日历Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ApsEveryWorkDayServiceImpl implements IApsEveryWorkDayService
{
    @Autowired
    private ApsEveryWorkDayMapper apsEveryWorkDayMapper;

    /**
     * 查询每日日历
     *
     * @param id 每日日历主键
     * @return 每日日历
     */
    @Override
    public ApsEveryWorkDay selectApsEveryWorkDayById(Long id)
    {
        return apsEveryWorkDayMapper.selectApsEveryWorkDayById(id);
    }

    /**
     * 查询每日日历列表
     *
     * @param apsEveryWorkDay 每日日历
     * @return 每日日历
     */
    @Override
    public List<ApsEveryWorkDay> selectApsEveryWorkDayList(ApsEveryWorkDay apsEveryWorkDay)
    {
        return apsEveryWorkDayMapper.selectApsEveryWorkDayList(apsEveryWorkDay);
    }

    /**
     * 新增每日日历
     *
     * @param apsEveryWorkDay 每日日历
     * @return 结果
     */
    @Override
    public int insertApsEveryWorkDay(ApsEveryWorkDay apsEveryWorkDay)
    {
        return apsEveryWorkDayMapper.insertApsEveryWorkDay(apsEveryWorkDay);
    }

    /**
     * 修改每日日历
     *
     * @param apsEveryWorkDay 每日日历
     * @return 结果
     */
    @Override
    public int updateApsEveryWorkDay(ApsEveryWorkDay apsEveryWorkDay)
    {
        return apsEveryWorkDayMapper.updateApsEveryWorkDay(apsEveryWorkDay);
    }

    /**
     * 批量删除每日日历
     *
     * @param ids 需要删除的每日日历主键
     * @return 结果
     */
    @Override
    public int deleteApsEveryWorkDayByIds(Long[] ids)
    {
        return apsEveryWorkDayMapper.deleteApsEveryWorkDayByIds(ids);
    }

    /**
     * 删除每日日历信息
     *
     * @param id 每日日历主键
     * @return 结果
     */
    @Override
    public int deleteApsEveryWorkDayById(Long id)
    {
        return apsEveryWorkDayMapper.deleteApsEveryWorkDayById(id);
    }

    @Override
    public int deleteApsEveryWorkDay(ApsEveryWorkDay apsEveryWorkDay) {
        return apsEveryWorkDayMapper.deleteApsEveryWorkDay(apsEveryWorkDay);
    }
}
