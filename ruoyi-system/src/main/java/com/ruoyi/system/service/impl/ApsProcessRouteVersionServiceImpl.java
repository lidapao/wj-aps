package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApsProcessRouteVersionMapper;
import com.ruoyi.system.domain.ApsProcessRouteVersion;
import com.ruoyi.system.service.IApsProcessRouteVersionService;

/**
 * 工艺路线版本Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-09
 */
@Service
public class ApsProcessRouteVersionServiceImpl implements IApsProcessRouteVersionService
{
    @Autowired
    private ApsProcessRouteVersionMapper apsProcessRouteVersionMapper;

    /**
     * 查询工艺路线版本
     *
     * @param id 工艺路线版本主键
     * @return 工艺路线版本
     */
    @Override
    public ApsProcessRouteVersion selectApsProcessRouteVersionById(Long id)
    {
        return apsProcessRouteVersionMapper.selectApsProcessRouteVersionById(id);
    }

    /**
     * 查询工艺路线版本列表
     *
     * @param apsProcessRouteVersion 工艺路线版本
     * @return 工艺路线版本
     */
    @Override
    public List<ApsProcessRouteVersion> selectApsProcessRouteVersionList(ApsProcessRouteVersion apsProcessRouteVersion)
    {
        return apsProcessRouteVersionMapper.selectApsProcessRouteVersionList(apsProcessRouteVersion);
    }

    /**
     * 新增工艺路线版本
     *
     * @param apsProcessRouteVersion 工艺路线版本
     * @return 结果
     */
    @Override
    public int insertApsProcessRouteVersion(ApsProcessRouteVersion apsProcessRouteVersion)
    {
        return apsProcessRouteVersionMapper.insertApsProcessRouteVersion(apsProcessRouteVersion);
    }

    /**
     * 修改工艺路线版本
     *
     * @param apsProcessRouteVersion 工艺路线版本
     * @return 结果
     */
    @Override
    public int updateApsProcessRouteVersion(ApsProcessRouteVersion apsProcessRouteVersion)
    {
        return apsProcessRouteVersionMapper.updateApsProcessRouteVersion(apsProcessRouteVersion);
    }

    /**
     * 批量删除工艺路线版本
     *
     * @param ids 需要删除的工艺路线版本主键
     * @return 结果
     */
    @Override
    public int deleteApsProcessRouteVersionByIds(Long[] ids)
    {
        return apsProcessRouteVersionMapper.deleteApsProcessRouteVersionByIds(ids);
    }

    /**
     * 删除工艺路线版本信息
     *
     * @param id 工艺路线版本主键
     * @return 结果
     */
    @Override
    public int deleteApsProcessRouteVersionById(Long id)
    {
        return apsProcessRouteVersionMapper.deleteApsProcessRouteVersionById(id);
    }

    @Override
    public List<ApsProcessRouteVersion> selectApsProcessRouteVersionByCode(String code) {
        return apsProcessRouteVersionMapper.selectApsProcessRouteVersionByCode(code);
    }
}
