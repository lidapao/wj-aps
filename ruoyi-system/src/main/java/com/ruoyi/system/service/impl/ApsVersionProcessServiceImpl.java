package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApsVersionProcessMapper;
import com.ruoyi.system.domain.ApsVersionProcess;
import com.ruoyi.system.service.IApsVersionProcessService;

/**
 * 工艺路线版本与工序关联表Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-10
 */
@Service
public class ApsVersionProcessServiceImpl implements IApsVersionProcessService
{
    @Autowired
    private ApsVersionProcessMapper apsVersionProcessMapper;

    /**
     * 查询工艺路线版本与工序关联表
     *
     * @param versionId 工艺路线版本与工序关联表主键
     * @return 工艺路线版本与工序关联表
     */
    @Override
    public List<ApsVersionProcess> selectApsVersionProcessByVersionId(Long versionId)
    {
        return apsVersionProcessMapper.selectApsVersionProcessByVersionId(versionId);
    }

    /**
     * 查询工艺路线版本与工序关联表列表
     *
     * @param apsVersionProcess 工艺路线版本与工序关联表
     * @return 工艺路线版本与工序关联表
     */
    @Override
    public List<ApsVersionProcess> selectApsVersionProcessList(ApsVersionProcess apsVersionProcess)
    {
        return apsVersionProcessMapper.selectApsVersionProcessList(apsVersionProcess);
    }

    /**
     * 新增工艺路线版本与工序关联表
     *
     * @param apsVersionProcess 工艺路线版本与工序关联表
     * @return 结果
     */
    @Override
    public int insertApsVersionProcess(ApsVersionProcess apsVersionProcess)
    {
        return apsVersionProcessMapper.insertApsVersionProcess(apsVersionProcess);
    }

    /**
     * 修改工艺路线版本与工序关联表
     *
     * @param apsVersionProcess 工艺路线版本与工序关联表
     * @return 结果
     */
    @Override
    public int updateApsVersionProcess(ApsVersionProcess apsVersionProcess)
    {
        return apsVersionProcessMapper.updateApsVersionProcess(apsVersionProcess);
    }

    /**
     * 批量删除工艺路线版本与工序关联表
     *
     * @param versionIds 需要删除的工艺路线版本与工序关联表主键
     * @return 结果
     */
    @Override
    public int deleteApsVersionProcessByVersionIds(Long[] versionIds)
    {
        return apsVersionProcessMapper.deleteApsVersionProcessByVersionIds(versionIds);
    }

    /**
     * 删除工艺路线版本与工序关联表信息
     *
     * @param versionId 工艺路线版本与工序关联表主键
     * @return 结果
     */
    @Override
    public int deleteApsVersionProcessByVersionId(Long versionId)
    {
        return apsVersionProcessMapper.deleteApsVersionProcessByVersionId(versionId);
    }
}
