package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApsClassConfigMapper;
import com.ruoyi.system.domain.ApsClassConfig;
import com.ruoyi.system.service.IApsClassConfigService;

/**
 * 排班规则Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@Service
public class ApsClassConfigServiceImpl implements IApsClassConfigService 
{
    @Autowired
    private ApsClassConfigMapper apsClassConfigMapper;

    /**
     * 查询排班规则
     * 
     * @param id 排班规则主键
     * @return 排班规则
     */
    @Override
    public ApsClassConfig selectApsClassConfigById(Long id)
    {
        return apsClassConfigMapper.selectApsClassConfigById(id);
    }

    /**
     * 查询排班规则列表
     * 
     * @param apsClassConfig 排班规则
     * @return 排班规则
     */
    @Override
    public List<ApsClassConfig> selectApsClassConfigList(ApsClassConfig apsClassConfig)
    {
        return apsClassConfigMapper.selectApsClassConfigList(apsClassConfig);
    }

    /**
     * 新增排班规则
     * 
     * @param apsClassConfig 排班规则
     * @return 结果
     */
    @Override
    public int insertApsClassConfig(ApsClassConfig apsClassConfig)
    {
        return apsClassConfigMapper.insertApsClassConfig(apsClassConfig);
    }

    /**
     * 修改排班规则
     * 
     * @param apsClassConfig 排班规则
     * @return 结果
     */
    @Override
    public int updateApsClassConfig(ApsClassConfig apsClassConfig)
    {
        return apsClassConfigMapper.updateApsClassConfig(apsClassConfig);
    }

    /**
     * 批量删除排班规则
     * 
     * @param ids 需要删除的排班规则主键
     * @return 结果
     */
    @Override
    public int deleteApsClassConfigByIds(Long[] ids)
    {
        return apsClassConfigMapper.deleteApsClassConfigByIds(ids);
    }

    /**
     * 删除排班规则信息
     * 
     * @param id 排班规则主键
     * @return 结果
     */
    @Override
    public int deleteApsClassConfigById(Long id)
    {
        return apsClassConfigMapper.deleteApsClassConfigById(id);
    }
}
