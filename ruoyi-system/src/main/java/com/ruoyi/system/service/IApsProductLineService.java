package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ApsProductLine;
import com.ruoyi.system.domain.vo.ApsLineProcessVo;

/**
 * 产线管理Service接口
 *
 * @author ruoyi
 * @date 2024-01-06
 */
public interface IApsProductLineService
{
    /**
     * 查询产线管理
     *
     * @param id 产线管理主键
     * @return 产线管理
     */
    public ApsProductLine selectApsProductLineById(Long id);

    /**
     * 查询产线管理列表
     *
     * @param apsProductLine 产线管理
     * @return 产线管理集合
     */
    public List<ApsProductLine> selectApsProductLineList(ApsProductLine apsProductLine);

    /**
     * 新增产线管理
     *
     * @param apsProductLine 产线管理
     * @return 结果
     */
    public int insertApsProductLine(ApsProductLine apsProductLine);

    /**
     * 修改产线管理
     *
     * @param apsProductLine 产线管理
     * @return 结果
     */
    public int updateApsProductLine(ApsProductLine apsProductLine);

    /**
     * 批量删除产线管理
     *
     * @param ids 需要删除的产线管理主键集合
     * @return 结果
     */
    public int deleteApsProductLineByIds(Long[] ids);

    /**
     * 删除产线管理信息
     *
     * @param id 产线管理主键
     * @return 结果
     */
    public int deleteApsProductLineById(Long id);

    public int bindLineAndProcess(ApsLineProcessVo apsLineProcessVo);
}
