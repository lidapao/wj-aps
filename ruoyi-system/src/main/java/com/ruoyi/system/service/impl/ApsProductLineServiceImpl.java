package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.ApsLineProcess;
import com.ruoyi.system.domain.ApsProcess;
import com.ruoyi.system.domain.vo.ApsLineProcessVo;
import com.ruoyi.system.mapper.ApsLineProcessMapper;
import com.ruoyi.system.mapper.ApsProcessMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApsProductLineMapper;
import com.ruoyi.system.domain.ApsProductLine;
import com.ruoyi.system.service.IApsProductLineService;

/**
 * 产线管理Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-06
 */
@Service
public class ApsProductLineServiceImpl implements IApsProductLineService
{
    @Autowired
    private ApsProductLineMapper apsProductLineMapper;

    @Autowired
    private ApsLineProcessMapper apsLineProcessMapper;

    @Autowired
    private ApsProcessMapper apsProcessMapper;

    /**
     * 查询产线管理
     *
     * @param id 产线管理主键
     * @return 产线管理
     */
    @Override
    public ApsProductLine selectApsProductLineById(Long id)
    {
        ApsProductLine apsProductLine = apsProductLineMapper.selectApsProductLineById(id);
        List<ApsProcess> processList = apsProductLine.getProcessList();
        List<ApsLineProcess> apsLineProcessList = apsLineProcessMapper.selectApsLineProcessByLineId(id);
        List<Long> processIds =
                apsLineProcessList.stream().map(ApsLineProcess::getProcessId).collect(Collectors.toList());
        for (Long processId : processIds) {
            ApsProcess apsProcess = apsProcessMapper.selectApsProcessById(processId);
            processList.add(apsProcess);
        }
        apsProductLine.setProcessList(processList);
        return apsProductLine;
    }

    /**
     * 查询产线管理列表
     *
     * @param apsProductLine 产线管理
     * @return 产线管理
     */
    @Override
    public List<ApsProductLine> selectApsProductLineList(ApsProductLine apsProductLine)
    {
        return apsProductLineMapper.selectApsProductLineList(apsProductLine);
    }

    /**
     * 新增产线管理
     *
     * @param apsProductLine 产线管理
     * @return 结果
     */
    @Override
    public int insertApsProductLine(ApsProductLine apsProductLine)
    {
        return apsProductLineMapper.insertApsProductLine(apsProductLine);
    }

    /**
     * 修改产线管理
     *
     * @param apsProductLine 产线管理
     * @return 结果
     */
    @Override
    public int updateApsProductLine(ApsProductLine apsProductLine)
    {
        return apsProductLineMapper.updateApsProductLine(apsProductLine);
    }

    /**
     * 批量删除产线管理
     *
     * @param ids 需要删除的产线管理主键
     * @return 结果
     */
    @Override
    public int deleteApsProductLineByIds(Long[] ids)
    {
        return apsProductLineMapper.deleteApsProductLineByIds(ids);
    }

    /**
     * 删除产线管理信息
     *
     * @param id 产线管理主键
     * @return 结果
     */
    @Override
    public int deleteApsProductLineById(Long id)
    {
        return apsProductLineMapper.deleteApsProductLineById(id);
    }

    @Override
    public int bindLineAndProcess(ApsLineProcessVo apsLineProcessVo) {
        apsLineProcessMapper.deleteApsLineProcessByLineId(apsLineProcessVo.getLineId());
        int i = 0;
        // 先删除原先的绑定关系
        if (StringUtils.isNotNull(apsLineProcessVo.getProcessIds())) {
            ApsLineProcess apsLineProcess = new ApsLineProcess();
            for (Long processId : apsLineProcessVo.getProcessIds()) {
                apsLineProcess.setLineId(apsLineProcessVo.getLineId());
                apsLineProcess.setProcessId(processId);
                i = i + apsLineProcessMapper.insertApsLineProcess(apsLineProcess);
            }
        }
        return i;
    }
}
