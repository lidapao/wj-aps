package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApsClassMapper;
import com.ruoyi.system.domain.ApsClass;
import com.ruoyi.system.service.IApsClassService;

/**
 * 班次管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ApsClassServiceImpl implements IApsClassService 
{
    @Autowired
    private ApsClassMapper apsClassMapper;

    /**
     * 查询班次管理
     * 
     * @param id 班次管理主键
     * @return 班次管理
     */
    @Override
    public ApsClass selectApsClassById(Long id)
    {
        return apsClassMapper.selectApsClassById(id);
    }

    /**
     * 查询班次管理列表
     * 
     * @param apsClass 班次管理
     * @return 班次管理
     */
    @Override
    public List<ApsClass> selectApsClassList(ApsClass apsClass)
    {
        return apsClassMapper.selectApsClassList(apsClass);
    }

    /**
     * 新增班次管理
     * 
     * @param apsClass 班次管理
     * @return 结果
     */
    @Override
    public int insertApsClass(ApsClass apsClass)
    {
        return apsClassMapper.insertApsClass(apsClass);
    }

    /**
     * 修改班次管理
     * 
     * @param apsClass 班次管理
     * @return 结果
     */
    @Override
    public int updateApsClass(ApsClass apsClass)
    {
        return apsClassMapper.updateApsClass(apsClass);
    }

    /**
     * 批量删除班次管理
     * 
     * @param ids 需要删除的班次管理主键
     * @return 结果
     */
    @Override
    public int deleteApsClassByIds(Long[] ids)
    {
        return apsClassMapper.deleteApsClassByIds(ids);
    }

    /**
     * 删除班次管理信息
     * 
     * @param id 班次管理主键
     * @return 结果
     */
    @Override
    public int deleteApsClassById(Long id)
    {
        return apsClassMapper.deleteApsClassById(id);
    }
}
