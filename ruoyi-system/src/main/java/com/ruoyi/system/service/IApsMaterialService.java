package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ApsMaterial;

/**
 * 物料管理Service接口
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IApsMaterialService 
{
    /**
     * 查询物料管理
     * 
     * @param id 物料管理主键
     * @return 物料管理
     */
    public ApsMaterial selectApsMaterialById(Long id);

    /**
     * 查询物料管理列表
     * 
     * @param apsMaterial 物料管理
     * @return 物料管理集合
     */
    public List<ApsMaterial> selectApsMaterialList(ApsMaterial apsMaterial);

    /**
     * 新增物料管理
     * 
     * @param apsMaterial 物料管理
     * @return 结果
     */
    public int insertApsMaterial(ApsMaterial apsMaterial);

    /**
     * 修改物料管理
     * 
     * @param apsMaterial 物料管理
     * @return 结果
     */
    public int updateApsMaterial(ApsMaterial apsMaterial);

    /**
     * 批量删除物料管理
     * 
     * @param ids 需要删除的物料管理主键集合
     * @return 结果
     */
    public int deleteApsMaterialByIds(Long[] ids);

    /**
     * 删除物料管理信息
     * 
     * @param id 物料管理主键
     * @return 结果
     */
    public int deleteApsMaterialById(Long id);
}
