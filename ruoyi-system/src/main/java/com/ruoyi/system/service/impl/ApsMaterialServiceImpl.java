package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApsMaterialMapper;
import com.ruoyi.system.domain.ApsMaterial;
import com.ruoyi.system.service.IApsMaterialService;

/**
 * 物料管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ApsMaterialServiceImpl implements IApsMaterialService 
{
    @Autowired
    private ApsMaterialMapper apsMaterialMapper;

    /**
     * 查询物料管理
     * 
     * @param id 物料管理主键
     * @return 物料管理
     */
    @Override
    public ApsMaterial selectApsMaterialById(Long id)
    {
        return apsMaterialMapper.selectApsMaterialById(id);
    }

    /**
     * 查询物料管理列表
     * 
     * @param apsMaterial 物料管理
     * @return 物料管理
     */
    @Override
    public List<ApsMaterial> selectApsMaterialList(ApsMaterial apsMaterial)
    {
        return apsMaterialMapper.selectApsMaterialList(apsMaterial);
    }

    /**
     * 新增物料管理
     * 
     * @param apsMaterial 物料管理
     * @return 结果
     */
    @Override
    public int insertApsMaterial(ApsMaterial apsMaterial)
    {
        apsMaterial.setCreateTime(DateUtils.getNowDate());
        return apsMaterialMapper.insertApsMaterial(apsMaterial);
    }

    /**
     * 修改物料管理
     * 
     * @param apsMaterial 物料管理
     * @return 结果
     */
    @Override
    public int updateApsMaterial(ApsMaterial apsMaterial)
    {
        return apsMaterialMapper.updateApsMaterial(apsMaterial);
    }

    /**
     * 批量删除物料管理
     * 
     * @param ids 需要删除的物料管理主键
     * @return 结果
     */
    @Override
    public int deleteApsMaterialByIds(Long[] ids)
    {
        return apsMaterialMapper.deleteApsMaterialByIds(ids);
    }

    /**
     * 删除物料管理信息
     * 
     * @param id 物料管理主键
     * @return 结果
     */
    @Override
    public int deleteApsMaterialById(Long id)
    {
        return apsMaterialMapper.deleteApsMaterialById(id);
    }
}
