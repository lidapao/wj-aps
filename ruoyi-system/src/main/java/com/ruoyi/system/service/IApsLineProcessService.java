package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ApsLineProcess;

/**
 * 产线工序关联表Service接口
 *
 * @author ruoyi
 * @date 2024-01-06
 */
public interface IApsLineProcessService
{
    /**
     * 查询产线工序关联表
     *
     * @param lineId 产线工序关联表主键
     * @return 产线工序关联表
     */
    public List<ApsLineProcess> selectApsLineProcessByLineId(Long lineId);

    /**
     * 查询产线工序关联表列表
     *
     * @param apsLineProcess 产线工序关联表
     * @return 产线工序关联表集合
     */
    public List<ApsLineProcess> selectApsLineProcessList(ApsLineProcess apsLineProcess);

    /**
     * 新增产线工序关联表
     *
     * @param apsLineProcess 产线工序关联表
     * @return 结果
     */
    public int insertApsLineProcess(ApsLineProcess apsLineProcess);

    /**
     * 修改产线工序关联表
     *
     * @param apsLineProcess 产线工序关联表
     * @return 结果
     */
    public int updateApsLineProcess(ApsLineProcess apsLineProcess);

    /**
     * 批量删除产线工序关联表
     *
     * @param lineIds 需要删除的产线工序关联表主键集合
     * @return 结果
     */
    public int deleteApsLineProcessByLineIds(Long[] lineIds);

    /**
     * 删除产线工序关联表信息
     *
     * @param lineId 产线工序关联表主键
     * @return 结果
     */
    public int deleteApsLineProcessByLineId(Long lineId);
}
