package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ApsProcess;
import com.ruoyi.system.domain.vo.ApsProcessVo;

/**
 * 产品工序Service接口
 *
 * @author ruoyi
 * @date 2024-01-06
 */
public interface IApsProcessService
{
    /**
     * 查询产品工序
     *
     * @param id 产品工序主键
     * @return 产品工序
     */
    public ApsProcess selectApsProcessById(Long id);

    /**
     * 查询产品工序列表
     *
     * @param apsProcess 产品工序
     * @return 产品工序集合
     */
    public List<ApsProcess> selectApsProcessList(ApsProcess apsProcess);

    /**
     * 新增产品工序
     *
     * @param apsProcess 产品工序
     * @return 结果
     */
    public int insertApsProcess(ApsProcess apsProcess);

    /**
     * 修改产品工序
     *
     * @param apsProcess 产品工序
     * @return 结果
     */
    public int updateApsProcess(ApsProcess apsProcess);

    /**
     * 批量删除产品工序
     *
     * @param ids 需要删除的产品工序主键集合
     * @return 结果
     */
    public int deleteApsProcessByIds(Long[] ids);

    /**
     * 删除产品工序信息
     *
     * @param id 产品工序主键
     * @return 结果
     */
    public int deleteApsProcessById(Long id);

    public List<ApsProcess> selectApsProcessListByVersionId(Long versionId);

    public int bindProcess(ApsProcessVo apsProcessVo);
}
