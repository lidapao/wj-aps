package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApsBomMapper;
import com.ruoyi.system.domain.ApsBom;
import com.ruoyi.system.service.IApsBomService;

/**
 * BOM管理Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-09
 */
@Service
public class ApsBomServiceImpl implements IApsBomService
{
    @Autowired
    private ApsBomMapper apsBomMapper;

    /**
     * 查询BOM管理
     *
     * @param id BOM管理主键
     * @return BOM管理
     */
    @Override
    public ApsBom selectApsBomById(Long id)
    {
        return apsBomMapper.selectApsBomById(id);
    }

    /**
     * 查询BOM管理列表
     *
     * @param apsBom BOM管理
     * @return BOM管理
     */
    @Override
    public List<ApsBom> selectApsBomList(ApsBom apsBom)
    {
        return apsBomMapper.selectApsBomList(apsBom);
    }

    /**
     * 新增BOM管理
     *
     * @param apsBom BOM管理
     * @return 结果
     */
    @Override
    public int insertApsBom(ApsBom apsBom)
    {
        apsBom.setCreateTime(DateUtils.getNowDate());
        return apsBomMapper.insertApsBom(apsBom);
    }

    /**
     * 修改BOM管理
     *
     * @param apsBom BOM管理
     * @return 结果
     */
    @Override
    public int updateApsBom(ApsBom apsBom)
    {
        return apsBomMapper.updateApsBom(apsBom);
    }

    /**
     * 批量删除BOM管理
     *
     * @param ids 需要删除的BOM管理主键
     * @return 结果
     */
    @Override
    public int deleteApsBomByIds(Long[] ids)
    {
        return apsBomMapper.deleteApsBomByIds(ids);
    }

    /**
     * 删除BOM管理信息
     *
     * @param id BOM管理主键
     * @return 结果
     */
    @Override
    public int deleteApsBomById(Long id)
    {
        return apsBomMapper.deleteApsBomById(id);
    }

    @Override
    public List<ApsBom> selectSysBomListUnique(ApsBom bom) {
        List<ApsBom> sysBoms = new ArrayList<>();
        bom.setType("成品");
        List<ApsBom> sysBoms1 = apsBomMapper.selectSysBomListUnique(bom);
        sysBoms.addAll(sysBoms1);
        bom.setType("自制件");
        List<ApsBom> sysBoms2 = apsBomMapper.selectSysBomListUnique(bom);
        sysBoms.addAll(sysBoms2);
        return sysBoms;
    }

    @Override
    public List<ApsBom> selectSysBomPageList(ApsBom sysBom) {
        sysBom.setParentId(0L);
        return apsBomMapper.selectApsBomList(sysBom);
    }

    @Override
    public List<ApsBom> selectSysBomTreeList(List<ApsBom> bomPageList, List<ApsBom> sysBoms) {
        List<ApsBom> collect =
                bomPageList.stream().map(bom -> {
                    bom.setChildren(getChildrenBoms(bom, sysBoms));
                    return bom;
                }).collect(Collectors.toList());
        return collect;
    }

    private List<ApsBom> getChildrenBoms(ApsBom root, List<ApsBom> all) {
        List<ApsBom> collect = all.stream().filter(sysBom -> Objects.equals(sysBom.getParentId(), root.getId())).collect(Collectors.toList());
        List<ApsBom> sysBomList = collect.stream().map(sysBom -> {
            sysBom.setChildren(getChildrenBoms(sysBom, all));
            return sysBom;
        }).collect(Collectors.toList());
        return sysBomList;
    }
}
