package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.ApsLineProcess;
import com.ruoyi.system.domain.ApsVersionProcess;
import com.ruoyi.system.domain.vo.ApsProcessVo;
import com.ruoyi.system.mapper.ApsVersionProcessMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApsProcessMapper;
import com.ruoyi.system.domain.ApsProcess;
import com.ruoyi.system.service.IApsProcessService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 产品工序Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-06
 */
@Service
public class ApsProcessServiceImpl implements IApsProcessService
{
    @Autowired
    private ApsProcessMapper apsProcessMapper;

    @Autowired
    private ApsVersionProcessMapper apsVersionProcessMapper;

    /**
     * 查询产品工序
     *
     * @param id 产品工序主键
     * @return 产品工序
     */
    @Override
    public ApsProcess selectApsProcessById(Long id)
    {
        return apsProcessMapper.selectApsProcessById(id);
    }

    /**
     * 查询产品工序列表
     *
     * @param apsProcess 产品工序
     * @return 产品工序
     */
    @Override
    public List<ApsProcess> selectApsProcessList(ApsProcess apsProcess)
    {
        return apsProcessMapper.selectApsProcessList(apsProcess);
    }

    /**
     * 新增产品工序
     *
     * @param apsProcess 产品工序
     * @return 结果
     */
    @Override
    public int insertApsProcess(ApsProcess apsProcess)
    {
        return apsProcessMapper.insertApsProcess(apsProcess);
    }

    /**
     * 修改产品工序
     *
     * @param apsProcess 产品工序
     * @return 结果
     */
    @Override
    public int updateApsProcess(ApsProcess apsProcess)
    {
        return apsProcessMapper.updateApsProcess(apsProcess);
    }

    /**
     * 批量删除产品工序
     *
     * @param ids 需要删除的产品工序主键
     * @return 结果
     */
    @Override
    public int deleteApsProcessByIds(Long[] ids)
    {
        return apsProcessMapper.deleteApsProcessByIds(ids);
    }

    /**
     * 删除产品工序信息
     *
     * @param id 产品工序主键
     * @return 结果
     */
    @Override
    public int deleteApsProcessById(Long id)
    {
        return apsProcessMapper.deleteApsProcessById(id);
    }

    @Override
    public List<ApsProcess> selectApsProcessListByVersionId(Long versionId) {
        List<ApsProcess> apsProcesses = new ArrayList<>();
        List<ApsVersionProcess> apsVersionProcesses = apsVersionProcessMapper.selectApsVersionProcessByVersionId(versionId);
        for (ApsVersionProcess apsVersionProcess : apsVersionProcesses) {
            ApsProcess apsProcess = apsProcessMapper.selectApsProcessById(apsVersionProcess.getProcessId());
            apsProcesses.add(apsProcess);
        }
        return apsProcesses;
    }

    @Override
    @Transactional
    public int bindProcess(ApsProcessVo apsProcessVo) {
        // 传需要绑定的工序Id -> 工序库里绑定
        int i = 0;
        Long[] processIds = apsProcessVo.getProcessIds();
        ApsProcess apsProcess = apsProcessVo.getApsProcess();
        ApsVersionProcess apsVersionProcess = new ApsVersionProcess();
        if (StringUtils.isNotEmpty(processIds)) {
            for (Long processId : processIds) {
                apsVersionProcess.setProcessId(processId);
                apsVersionProcess.setVersionId(apsProcessVo.getProcessVersionId());
                i = i + apsVersionProcessMapper.insertApsVersionProcess(apsVersionProcess);
            }
        } else if (StringUtils.isNotNull(apsProcess)) {
            apsProcessMapper.insertApsProcess(apsProcess);
            apsVersionProcess.setVersionId(apsProcessVo.getProcessVersionId());
            apsVersionProcess.setProcessId(apsProcess.getId());
            i = i + apsVersionProcessMapper.insertApsVersionProcess(apsVersionProcess);
        }
        return i;
    }
}
