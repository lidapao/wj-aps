package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ApsClassConfig;

/**
 * 排班规则Service接口
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IApsClassConfigService 
{
    /**
     * 查询排班规则
     * 
     * @param id 排班规则主键
     * @return 排班规则
     */
    public ApsClassConfig selectApsClassConfigById(Long id);

    /**
     * 查询排班规则列表
     * 
     * @param apsClassConfig 排班规则
     * @return 排班规则集合
     */
    public List<ApsClassConfig> selectApsClassConfigList(ApsClassConfig apsClassConfig);

    /**
     * 新增排班规则
     * 
     * @param apsClassConfig 排班规则
     * @return 结果
     */
    public int insertApsClassConfig(ApsClassConfig apsClassConfig);

    /**
     * 修改排班规则
     * 
     * @param apsClassConfig 排班规则
     * @return 结果
     */
    public int updateApsClassConfig(ApsClassConfig apsClassConfig);

    /**
     * 批量删除排班规则
     * 
     * @param ids 需要删除的排班规则主键集合
     * @return 结果
     */
    public int deleteApsClassConfigByIds(Long[] ids);

    /**
     * 删除排班规则信息
     * 
     * @param id 排班规则主键
     * @return 结果
     */
    public int deleteApsClassConfigById(Long id);
}
