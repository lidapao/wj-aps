package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ApsBom;

/**
 * BOM管理Service接口
 *
 * @author ruoyi
 * @date 2024-01-09
 */
public interface IApsBomService
{
    /**
     * 查询BOM管理
     *
     * @param id BOM管理主键
     * @return BOM管理
     */
    public ApsBom selectApsBomById(Long id);

    /**
     * 查询BOM管理列表
     *
     * @param apsBom BOM管理
     * @return BOM管理集合
     */
    public List<ApsBom> selectApsBomList(ApsBom apsBom);

    /**
     * 新增BOM管理
     *
     * @param apsBom BOM管理
     * @return 结果
     */
    public int insertApsBom(ApsBom apsBom);

    /**
     * 修改BOM管理
     *
     * @param apsBom BOM管理
     * @return 结果
     */
    public int updateApsBom(ApsBom apsBom);

    /**
     * 批量删除BOM管理
     *
     * @param ids 需要删除的BOM管理主键集合
     * @return 结果
     */
    public int deleteApsBomByIds(Long[] ids);

    /**
     * 删除BOM管理信息
     *
     * @param id BOM管理主键
     * @return 结果
     */
    public int deleteApsBomById(Long id);

    public List<ApsBom> selectSysBomListUnique(ApsBom bom);

    public List<ApsBom> selectSysBomPageList(ApsBom sysBom);

    public List<ApsBom> selectSysBomTreeList(List<ApsBom> bomPageList, List<ApsBom> sysBoms);
}
