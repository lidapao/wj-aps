package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ApsEveryWorkDay;

/**
 * 每日日历Service接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IApsEveryWorkDayService
{
    /**
     * 查询每日日历
     *
     * @param id 每日日历主键
     * @return 每日日历
     */
    public ApsEveryWorkDay selectApsEveryWorkDayById(Long id);

    /**
     * 查询每日日历列表
     *
     * @param apsEveryWorkDay 每日日历
     * @return 每日日历集合
     */
    public List<ApsEveryWorkDay> selectApsEveryWorkDayList(ApsEveryWorkDay apsEveryWorkDay);

    /**
     * 新增每日日历
     *
     * @param apsEveryWorkDay 每日日历
     * @return 结果
     */
    public int insertApsEveryWorkDay(ApsEveryWorkDay apsEveryWorkDay);

    /**
     * 修改每日日历
     *
     * @param apsEveryWorkDay 每日日历
     * @return 结果
     */
    public int updateApsEveryWorkDay(ApsEveryWorkDay apsEveryWorkDay);

    /**
     * 批量删除每日日历
     *
     * @param ids 需要删除的每日日历主键集合
     * @return 结果
     */
    public int deleteApsEveryWorkDayByIds(Long[] ids);

    /**
     * 删除每日日历信息
     *
     * @param id 每日日历主键
     * @return 结果
     */
    public int deleteApsEveryWorkDayById(Long id);

    public int deleteApsEveryWorkDay(ApsEveryWorkDay apsEveryWorkDay);
}
