package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物料管理对象 aps_material
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public class ApsMaterial extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 物料id */
    private Long id;

    /** 物料编码 */
    @Excel(name = "物料编码")
    private String code;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String name;

    /** 物料类型 */
    @Excel(name = "物料类型")
    private String type;

    /** 图号 */
    @Excel(name = "图号")
    private String drawNo;

    /** 仓位 */
    @Excel(name = "仓位")
    private String position;

    /** 每用量 */
    @Excel(name = "每用量")
    private BigDecimal perUsage;

    /** 最大库存量 */
    @Excel(name = "最大库存量")
    private Long maxStock;

    /** 最小库存量 */
    @Excel(name = "最小库存量")
    private Long minStock;

    /** 物料状态 0：缺货，待采购 1:采购中 2:已采购 待入库 3:足料 4:已领 */
    @Excel(name = "物料状态 0：缺货，待采购 1:采购中 2:已采购 待入库 3:足料 4:已领")
    private Long materialStatus;

    /** 真实数量 */
    @Excel(name = "真实数量")
    private BigDecimal realNumber;

    /** 含税单价 */
    @Excel(name = "含税单价")
    private BigDecimal taxUnitPrice;

    /** 未含税单价 */
    @Excel(name = "未含税单价")
    private BigDecimal unitPriceWithoutTax;

    /** 系统计算量 */
    @Excel(name = "系统计算量")
    private BigDecimal systemCalculateNumber;

    /** 需求量 */
    @Excel(name = "需求量")
    private BigDecimal demandNumber;

    /** 采购中的的数量 */
    @Excel(name = "采购中的的数量")
    private BigDecimal procurementNumber;

    /** 需要采购的数量 */
    @Excel(name = "需要采购的数量")
    private BigDecimal demandProcurementNumber;

    /** 分类id */
    @Excel(name = "分类id")
    private Long categoryId;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String categoryName;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 供应商名称 */
    @Excel(name = "供应商名称")
    private String supplierName;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 结算方式 */
    @Excel(name = "结算方式")
    private Long settlementMethod;

    /** 开票方式 */
    @Excel(name = "开票方式")
    private Long billingMethod;

    /** 规格型号 */
    @Excel(name = "规格型号")
    private Long specs;

    /** 主制车间 */
    @Excel(name = "主制车间")
    private String workShop;

    public String getWorkShop() {
        return workShop;
    }

    public void setWorkShop(String workShop) {
        this.workShop = workShop;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setDrawNo(String drawNo)
    {
        this.drawNo = drawNo;
    }

    public String getDrawNo()
    {
        return drawNo;
    }
    public void setPosition(String position)
    {
        this.position = position;
    }

    public String getPosition()
    {
        return position;
    }
    public void setPerUsage(BigDecimal perUsage)
    {
        this.perUsage = perUsage;
    }

    public BigDecimal getPerUsage()
    {
        return perUsage;
    }
    public void setMaxStock(Long maxStock)
    {
        this.maxStock = maxStock;
    }

    public Long getMaxStock()
    {
        return maxStock;
    }
    public void setMinStock(Long minStock)
    {
        this.minStock = minStock;
    }

    public Long getMinStock()
    {
        return minStock;
    }
    public void setMaterialStatus(Long materialStatus)
    {
        this.materialStatus = materialStatus;
    }

    public Long getMaterialStatus()
    {
        return materialStatus;
    }
    public void setRealNumber(BigDecimal realNumber)
    {
        this.realNumber = realNumber;
    }

    public BigDecimal getRealNumber()
    {
        return realNumber;
    }
    public void setTaxUnitPrice(BigDecimal taxUnitPrice)
    {
        this.taxUnitPrice = taxUnitPrice;
    }

    public BigDecimal getTaxUnitPrice()
    {
        return taxUnitPrice;
    }
    public void setUnitPriceWithoutTax(BigDecimal unitPriceWithoutTax)
    {
        this.unitPriceWithoutTax = unitPriceWithoutTax;
    }

    public BigDecimal getUnitPriceWithoutTax()
    {
        return unitPriceWithoutTax;
    }
    public void setSystemCalculateNumber(BigDecimal systemCalculateNumber)
    {
        this.systemCalculateNumber = systemCalculateNumber;
    }

    public BigDecimal getSystemCalculateNumber()
    {
        return systemCalculateNumber;
    }
    public void setDemandNumber(BigDecimal demandNumber)
    {
        this.demandNumber = demandNumber;
    }

    public BigDecimal getDemandNumber()
    {
        return demandNumber;
    }
    public void setProcurementNumber(BigDecimal procurementNumber)
    {
        this.procurementNumber = procurementNumber;
    }

    public BigDecimal getProcurementNumber()
    {
        return procurementNumber;
    }
    public void setDemandProcurementNumber(BigDecimal demandProcurementNumber)
    {
        this.demandProcurementNumber = demandProcurementNumber;
    }

    public BigDecimal getDemandProcurementNumber()
    {
        return demandProcurementNumber;
    }
    public void setCategoryId(Long categoryId)
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId()
    {
        return categoryId;
    }
    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

    public String getCategoryName()
    {
        return categoryName;
    }
    public void setSupplierId(Long supplierId)
    {
        this.supplierId = supplierId;
    }

    public Long getSupplierId()
    {
        return supplierId;
    }
    public void setSupplierName(String supplierName)
    {
        this.supplierName = supplierName;
    }

    public String getSupplierName()
    {
        return supplierName;
    }
    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public String getUnit()
    {
        return unit;
    }
    public void setSettlementMethod(Long settlementMethod)
    {
        this.settlementMethod = settlementMethod;
    }

    public Long getSettlementMethod()
    {
        return settlementMethod;
    }
    public void setBillingMethod(Long billingMethod)
    {
        this.billingMethod = billingMethod;
    }

    public Long getBillingMethod()
    {
        return billingMethod;
    }
    public void setSpecs(Long specs)
    {
        this.specs = specs;
    }

    public Long getSpecs()
    {
        return specs;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("type", getType())
            .append("drawNo", getDrawNo())
            .append("position", getPosition())
            .append("perUsage", getPerUsage())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("maxStock", getMaxStock())
            .append("minStock", getMinStock())
            .append("materialStatus", getMaterialStatus())
            .append("realNumber", getRealNumber())
            .append("taxUnitPrice", getTaxUnitPrice())
            .append("unitPriceWithoutTax", getUnitPriceWithoutTax())
            .append("systemCalculateNumber", getSystemCalculateNumber())
            .append("demandNumber", getDemandNumber())
            .append("procurementNumber", getProcurementNumber())
            .append("demandProcurementNumber", getDemandProcurementNumber())
            .append("categoryId", getCategoryId())
            .append("categoryName", getCategoryName())
            .append("supplierId", getSupplierId())
            .append("supplierName", getSupplierName())
            .append("unit", getUnit())
            .append("settlementMethod", getSettlementMethod())
            .append("billingMethod", getBillingMethod())
            .append("specs", getSpecs())
            .append("workShop", getWorkShop())
            .toString();
    }
}
