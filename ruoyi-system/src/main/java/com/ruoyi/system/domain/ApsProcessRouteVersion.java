package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 工艺路线版本对象 aps_process_route_version
 *
 * @author ruoyi
 * @date 2024-01-09
 */
public class ApsProcessRouteVersion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 工艺路线版本id */
    private Long id;

    /** 工艺类型(0标准工艺1临时工艺) */
    @Excel(name = "工艺类型(0标准工艺1临时工艺)")
    private Long type;

    /** 工艺编码 */
    @Excel(name = "工艺编码")
    private String code;

    /** 工艺名称 */
    @Excel(name = "工艺名称")
    private String name;

    /** 版本号 */
    @Excel(name = "版本号")
    private String version;

    /** 是否启用(0禁用1启用) */
    @Excel(name = "是否启用(0禁用1启用)")
    private Long flag;

    /** 工艺路线 */
    @Excel(name = "工艺路线")
    private String route;

    /** 状态(0待审核1审核中2已审核) */
    @Excel(name = "状态(0待审核1审核中2已审核)")
    private Long state;

    /** 物料编码 */
    @Excel(name = "物料编码")
    private String materialCode;

    private List<ApsProcess> processList;

    public List<ApsProcess> getProcessList() {
        return processList;
    }

    public void setProcessList(List<ApsProcess> processList) {
        this.processList = processList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setType(Long type)
    {
        this.type = type;
    }

    public Long getType()
    {
        return type;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getVersion()
    {
        return version;
    }
    public void setFlag(Long flag)
    {
        this.flag = flag;
    }

    public Long getFlag()
    {
        return flag;
    }
    public void setRoute(String route)
    {
        this.route = route;
    }

    public String getRoute()
    {
        return route;
    }
    public void setState(Long state)
    {
        this.state = state;
    }

    public Long getState()
    {
        return state;
    }
    public void setMaterialCode(String materialCode)
    {
        this.materialCode = materialCode;
    }

    public String getMaterialCode()
    {
        return materialCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("code", getCode())
            .append("name", getName())
            .append("version", getVersion())
            .append("flag", getFlag())
            .append("route", getRoute())
            .append("state", getState())
            .append("materialCode", getMaterialCode())
            .toString();
    }
}
