package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 排班规则对象 aps_class_config
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public class ApsClassConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 排班规则id */
    private Long id;

    /** 开始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 结束日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 优先级(数字大的优先级高) */
    @Excel(name = "优先级(数字大的优先级高)")
    private Long priority;

    /** 排班方式(0固定1轮班) */
    @Excel(name = "排班方式(0固定1轮班)")
    private Long classType;

    /** 人员班组 */
    @Excel(name = "人员班组")
    private String team;

    /** 班次 */
    @Excel(name = "班次")
    private String className;

    /** 生产线id */
    @Excel(name = "生产线id")
    private Long lineId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStartDate(Date startDate) 
    {
        this.startDate = startDate;
    }

    public Date getStartDate() 
    {
        return startDate;
    }
    public void setEndDate(Date endDate) 
    {
        this.endDate = endDate;
    }

    public Date getEndDate() 
    {
        return endDate;
    }
    public void setPriority(Long priority) 
    {
        this.priority = priority;
    }

    public Long getPriority() 
    {
        return priority;
    }
    public void setClassType(Long classType) 
    {
        this.classType = classType;
    }

    public Long getClassType() 
    {
        return classType;
    }
    public void setTeam(String team) 
    {
        this.team = team;
    }

    public String getTeam() 
    {
        return team;
    }
    public void setClassName(String className) 
    {
        this.className = className;
    }

    public String getClassName() 
    {
        return className;
    }
    public void setLineId(Long lineId) 
    {
        this.lineId = lineId;
    }

    public Long getLineId() 
    {
        return lineId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("startDate", getStartDate())
            .append("endDate", getEndDate())
            .append("priority", getPriority())
            .append("classType", getClassType())
            .append("team", getTeam())
            .append("className", getClassName())
            .append("lineId", getLineId())
            .toString();
    }
}
