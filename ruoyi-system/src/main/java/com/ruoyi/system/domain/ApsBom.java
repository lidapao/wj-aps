package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * BOM管理对象 aps_bom
 *
 * @author ruoyi
 * @date 2024-01-09
 */
public class ApsBom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** BOM id */
    private Long id;

    /** 物料编码 */
    @Excel(name = "物料编码")
    private String code;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String name;

    /** 父级id */
    @Excel(name = "父级id")
    private Long parentId;

    /** 物料类型 */
    @Excel(name = "物料类型")
    private String type;

    /** 图号 */
    @Excel(name = "图号")
    private String drawNo;

    /** 仓位 */
    @Excel(name = "仓位")
    private String position;

    /** 每用量 */
    @Excel(name = "每用量")
    private BigDecimal perUsage;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 物料状态 0：缺货，待采购 1:采购中 2:已采购 待入库 3:足料 4:已领 */
    @Excel(name = "物料状态 0：缺货，待采购 1:采购中 2:已采购 待入库 3:足料 4:已领")
    private Long materialStatus;

    private List<ApsBom> children;

    private List<ApsProcessRouteVersion> processRouteVersionList;

    public List<ApsProcessRouteVersion> getProcessRouteVersionList() {
        return processRouteVersionList;
    }

    public void setProcessRouteVersionList(List<ApsProcessRouteVersion> processRouteVersionList) {
        this.processRouteVersionList = processRouteVersionList;
    }

    public List<ApsBom> getChildren() {
        return children;
    }

    public void setChildren(List<ApsBom> children) {
        this.children = children;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Long getParentId()
    {
        return parentId;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setDrawNo(String drawNo)
    {
        this.drawNo = drawNo;
    }

    public String getDrawNo()
    {
        return drawNo;
    }
    public void setPosition(String position)
    {
        this.position = position;
    }

    public String getPosition()
    {
        return position;
    }
    public void setPerUsage(BigDecimal perUsage)
    {
        this.perUsage = perUsage;
    }

    public BigDecimal getPerUsage()
    {
        return perUsage;
    }
    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public String getUnit()
    {
        return unit;
    }
    public void setMaterialStatus(Long materialStatus)
    {
        this.materialStatus = materialStatus;
    }

    public Long getMaterialStatus()
    {
        return materialStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("parentId", getParentId())
            .append("type", getType())
            .append("drawNo", getDrawNo())
            .append("position", getPosition())
            .append("perUsage", getPerUsage())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("unit", getUnit())
            .append("materialStatus", getMaterialStatus())
            .toString();
    }
}
