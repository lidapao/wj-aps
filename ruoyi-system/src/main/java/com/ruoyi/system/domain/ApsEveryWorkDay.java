package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 每日日历对象 aps_every_work_day
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public class ApsEveryWorkDay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 日历id */
    private Long id;

    /** 名称(取当天日期) */
    @Excel(name = "名称(取当天日期)")
    private String name;

    /** 日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /** 优先级 */
    @Excel(name = "优先级")
    private Long priority;

    /** 星期几 */
    @Excel(name = "星期几")
    private String dayOfWeek;

    /** 班次 */
    @Excel(name = "班次")
    private List<String> className;

    /** 部门id */
    @ApiModelProperty(name = "deptId",value = "部门id",required = true)
    @Excel(name = "部门id")
    private Long deptId;

    /** 祖级列表 */
    @ApiModelProperty(name = "ancestors",value = "祖级列表",required = true)
    @Excel(name = "祖级列表")
    private String ancestors;

    /** 产线id */
    @ApiModelProperty(name = "lineId",value = "产线id(有就传,无不传)",required = true)
    @Excel(name = "产线id")
    private Long lineId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setDate(Date date)
    {
        this.date = date;
    }

    public Date getDate()
    {
        return date;
    }
    public void setPriority(Long priority)
    {
        this.priority = priority;
    }

    public Long getPriority()
    {
        return priority;
    }
    public void setDayOfWeek(String dayOfWeek)
    {
        this.dayOfWeek = dayOfWeek;
    }

    public String getDayOfWeek()
    {
        return dayOfWeek;
    }
    public void setClassName(List<String> className)
    {
        this.className = className;
    }

    public List<String> getClassName()
    {
        return className;
    }
    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    public Long getDeptId()
    {
        return deptId;
    }
    public void setAncestors(String ancestors)
    {
        this.ancestors = ancestors;
    }

    public String getAncestors()
    {
        return ancestors;
    }
    public void setLineId(Long lineId)
    {
        this.lineId = lineId;
    }

    public Long getLineId()
    {
        return lineId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("date", getDate())
            .append("priority", getPriority())
            .append("dayOfWeek", getDayOfWeek())
            .append("className", getClassName())
            .append("deptId", getDeptId())
            .append("ancestors", getAncestors())
            .append("lineId", getLineId())
            .toString();
    }
}
