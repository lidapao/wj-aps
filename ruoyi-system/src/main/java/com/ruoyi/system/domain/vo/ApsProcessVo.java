package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ApsProcess;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 产品工序对象 aps_process
 *
 * @author ruoyi
 * @date 2024-01-06
 */
public class ApsProcessVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

   private ApsProcess apsProcess;

    private Long processVersionId;

    private Long[] processIds;

    public ApsProcess getApsProcess() {
        return apsProcess;
    }

    public void setApsProcess(ApsProcess apsProcess) {
        this.apsProcess = apsProcess;
    }

    public Long[] getProcessIds() {
        return processIds;
    }

    public void setProcessIds(Long[] processIds) {
        this.processIds = processIds;
    }

    public Long getProcessVersionId() {
        return processVersionId;
    }

    public void setProcessVersionId(Long processVersionId) {
        this.processVersionId = processVersionId;
    }

    @Override
    public String toString() {
        return "ApsProcessVo{" +
                "apsProcess=" + apsProcess +
                ", processVersionId=" + processVersionId +
                ", processIds=" + Arrays.toString(processIds) +
                '}';
    }
}
