package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 工艺路线版本与工序关联表对象 aps_version_process
 * 
 * @author ruoyi
 * @date 2024-01-10
 */
public class ApsVersionProcess extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 工艺路线版本id */
    private Long versionId;

    /** 工序id */
    @Excel(name = "工序id")
    private Long processId;

    public void setVersionId(Long versionId) 
    {
        this.versionId = versionId;
    }

    public Long getVersionId() 
    {
        return versionId;
    }
    public void setProcessId(Long processId) 
    {
        this.processId = processId;
    }

    public Long getProcessId() 
    {
        return processId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("versionId", getVersionId())
            .append("processId", getProcessId())
            .toString();
    }
}
