package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 产线管理对象 aps_product_line
 *
 * @author ruoyi
 * @date 2024-01-06
 */
public class ApsProductLine extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 产线id */
    private Long id;

    /** 产线名称 */
    @Excel(name = "产线名称")
    private String name;

    /** 车间id */
    @Excel(name = "车间id")
    private Long deptId;

    /** 车间名称 */
    @Excel(name = "车间名称")
    private String deptName;

    /** 负责人id */
    @Excel(name = "负责人id")
    private Long directorId;

    /** 负责人姓名 */
    @Excel(name = "负责人姓名")
    private String directorName;

    /** 负责人电话 */
    @Excel(name = "负责人电话")
    private String directorPhone;

    /** 班组类型(单人/班组) */
    @Excel(name = "班组类型(单人/班组)")
    private String classType;

    /** 组织人数 */
    @Excel(name = "组织人数")
    private Long peopleNum;

    /** 排产天数 */
    @Excel(name = "排产天数")
    private Long productDays;

    private List<ApsProcess> processList;

    public List<ApsProcess> getProcessList() {
        return processList;
    }

    public void setProcessList(List<ApsProcess> processList) {
        this.processList = processList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    public Long getDeptId()
    {
        return deptId;
    }
    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    public String getDeptName()
    {
        return deptName;
    }
    public void setDirectorId(Long directorId)
    {
        this.directorId = directorId;
    }

    public Long getDirectorId()
    {
        return directorId;
    }
    public void setDirectorName(String directorName)
    {
        this.directorName = directorName;
    }

    public String getDirectorName()
    {
        return directorName;
    }
    public void setDirectorPhone(String directorPhone)
    {
        this.directorPhone = directorPhone;
    }

    public String getDirectorPhone()
    {
        return directorPhone;
    }
    public void setClassType(String classType)
    {
        this.classType = classType;
    }

    public String getClassType()
    {
        return classType;
    }
    public void setPeopleNum(Long peopleNum)
    {
        this.peopleNum = peopleNum;
    }

    public Long getPeopleNum()
    {
        return peopleNum;
    }
    public void setProductDays(Long productDays)
    {
        this.productDays = productDays;
    }

    public Long getProductDays()
    {
        return productDays;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("deptId", getDeptId())
            .append("deptName", getDeptName())
            .append("directorId", getDirectorId())
            .append("directorName", getDirectorName())
            .append("directorPhone", getDirectorPhone())
            .append("classType", getClassType())
            .append("peopleNum", getPeopleNum())
            .append("productDays", getProductDays())
            .toString();
    }
}
