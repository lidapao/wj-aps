package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品工序对象 aps_process
 * 
 * @author ruoyi
 * @date 2024-01-06
 */
public class ApsProcess extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 工序主键 */
    private Long id;

    /** 工序编码 */
    @Excel(name = "工序编码")
    private String code;

    /** 工序名称 */
    @Excel(name = "工序名称")
    private String name;

    /** 单件额定准备时间(分钟) */
    @Excel(name = "单件额定准备时间(分钟)")
    private BigDecimal prepareTime;

    /** 单件额定加工时间(分钟) */
    @Excel(name = "单件额定加工时间(分钟)")
    private BigDecimal productTime;

    /** 单件额定结束时间(分钟) */
    @Excel(name = "单件额定结束时间(分钟)")
    private BigDecimal afterTime;

    /** 工序类型(0人工,1设备加工) */
    @Excel(name = "工序类型(0人工,1设备加工)")
    private Long type;

    /** 文件地址 */
    @Excel(name = "文件地址")
    private String url;

    /** 前道工序名称 */
    @Excel(name = "前道工序名称")
    private String previousProcess;

    /** 约束类型(0结束后开始,1同时开始) */
    @Excel(name = "约束类型(0结束后开始,1同时开始)")
    private Long conType;

    /** 前道工序id */
    @Excel(name = "前道工序id")
    private Long previousProcessId;

    /** 工艺代码 */
    @Excel(name = "工艺代码")
    private String processShort;

    /** 工种 */
    @Excel(name = "工种")
    private Long workType;

    /** 额定工时 */
    @Excel(name = "额定工时")
    private Long standardWorkHour;

    /** 工时计算方式(计件/计时) */
    @Excel(name = "工时计算方式(计件/计时)")
    private Long workHourType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPrepareTime(BigDecimal prepareTime) 
    {
        this.prepareTime = prepareTime;
    }

    public BigDecimal getPrepareTime() 
    {
        return prepareTime;
    }
    public void setProductTime(BigDecimal productTime) 
    {
        this.productTime = productTime;
    }

    public BigDecimal getProductTime() 
    {
        return productTime;
    }
    public void setAfterTime(BigDecimal afterTime) 
    {
        this.afterTime = afterTime;
    }

    public BigDecimal getAfterTime() 
    {
        return afterTime;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setPreviousProcess(String previousProcess) 
    {
        this.previousProcess = previousProcess;
    }

    public String getPreviousProcess() 
    {
        return previousProcess;
    }
    public void setConType(Long conType) 
    {
        this.conType = conType;
    }

    public Long getConType() 
    {
        return conType;
    }
    public void setPreviousProcessId(Long previousProcessId) 
    {
        this.previousProcessId = previousProcessId;
    }

    public Long getPreviousProcessId() 
    {
        return previousProcessId;
    }
    public void setProcessShort(String processShort) 
    {
        this.processShort = processShort;
    }

    public String getProcessShort() 
    {
        return processShort;
    }
    public void setWorkType(Long workType) 
    {
        this.workType = workType;
    }

    public Long getWorkType() 
    {
        return workType;
    }
    public void setStandardWorkHour(Long standardWorkHour) 
    {
        this.standardWorkHour = standardWorkHour;
    }

    public Long getStandardWorkHour() 
    {
        return standardWorkHour;
    }
    public void setWorkHourType(Long workHourType) 
    {
        this.workHourType = workHourType;
    }

    public Long getWorkHourType() 
    {
        return workHourType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("prepareTime", getPrepareTime())
            .append("productTime", getProductTime())
            .append("afterTime", getAfterTime())
            .append("type", getType())
            .append("url", getUrl())
            .append("previousProcess", getPreviousProcess())
            .append("conType", getConType())
            .append("previousProcessId", getPreviousProcessId())
            .append("processShort", getProcessShort())
            .append("workType", getWorkType())
            .append("standardWorkHour", getStandardWorkHour())
            .append("workHourType", getWorkHourType())
            .toString();
    }
}
