package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 产线工序关联表对象 aps_line_process
 *
 * @author ruoyi
 * @date 2024-01-06
 */
public class ApsLineProcessVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 生产线id */
    @Excel(name = "生产线id")
    private Long lineId;

    /** 工序id */
    @Excel(name = "工序id")
    private Long processId;

    private Long[] processIds;

    public Long[] getProcessIds() {
        return processIds;
    }

    public void setProcessIds(Long[] processIds) {
        this.processIds = processIds;
    }

    public void setLineId(Long lineId)
    {
        this.lineId = lineId;
    }

    public Long getLineId()
    {
        return lineId;
    }
    public void setProcessId(Long processId)
    {
        this.processId = processId;
    }

    public Long getProcessId()
    {
        return processId;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("lineId", getLineId())
            .append("processId", getProcessId())
            .toString();
    }
}
