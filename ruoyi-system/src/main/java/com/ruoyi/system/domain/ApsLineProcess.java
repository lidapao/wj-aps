package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产线工序关联表对象 aps_line_process
 * 
 * @author ruoyi
 * @date 2024-01-06
 */
public class ApsLineProcess extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 生产线id */
    @Excel(name = "生产线id")
    private Long lineId;

    /** 工序id */
    @Excel(name = "工序id")
    private Long processId;

    /** 自定义定额工时 */
    @Excel(name = "自定义定额工时")
    private Long standardWorkHour;

    public void setLineId(Long lineId) 
    {
        this.lineId = lineId;
    }

    public Long getLineId() 
    {
        return lineId;
    }
    public void setProcessId(Long processId) 
    {
        this.processId = processId;
    }

    public Long getProcessId() 
    {
        return processId;
    }
    public void setStandardWorkHour(Long standardWorkHour) 
    {
        this.standardWorkHour = standardWorkHour;
    }

    public Long getStandardWorkHour() 
    {
        return standardWorkHour;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("lineId", getLineId())
            .append("processId", getProcessId())
            .append("standardWorkHour", getStandardWorkHour())
            .toString();
    }
}
