package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 工作日历设置规则对象 aps_work_day_config
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public class ApsWorkDayConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 工厂日历id */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 产线id */
    @ApiModelProperty(value = "产线id(有就传,无不传)",name = "lineId",required = true)
    @Excel(name = "产线id")
    private Long lineId;

    /** 起始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 结束日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 优先级(数字大的优先级高) */
    @Excel(name = "优先级(数字大的优先级高)")
    private Long priority;

    /** 星期一班次 */
    @Excel(name = "星期一班次")
    private List<String> monday;

    /** 星期二班次 */
    @Excel(name = "星期二班次")
    private List<String> tuesday;

    /** 星期三班次 */
    @Excel(name = "星期三班次")
    private List<String> wednesday;

    /** 星期四班次 */
    @Excel(name = "星期四班次")
    private List<String> thursday;

    /** 星期五班次 */
    @Excel(name = "星期五班次")
    private List<String> friday;

    /** 星期六班次 */
    @Excel(name = "星期六班次")
    private List<String> saturday;

    /** 星期日班次 */
    @Excel(name = "星期日班次")
    private List<String> sunday;

    /** 部门id */
    @ApiModelProperty(value = "部门id(有就传,无不传)",name = "deptId",required = true)
    @Excel(name = "部门id")
    private Long deptId;

    /** 祖级列表 */
    @ApiModelProperty(value = "祖级列表(有就传,无不传)",name = "ancestors",required = true)
    @Excel(name = "祖级列表")
    private String ancestors;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public Long getLineId() {
        return lineId;
    }

    public void setLineId(Long lineId) {
        this.lineId = lineId;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getStartDate()
    {
        return startDate;
    }
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }
    public void setPriority(Long priority)
    {
        this.priority = priority;
    }

    public Long getPriority()
    {
        return priority;
    }
    public void setMonday(List<String> Monday)
    {
        this.monday = Monday;
    }

    public List<String> getMonday()
    {
        return monday;
    }
    public void setTuesday(List<String> Tuesday)
    {
        this.tuesday = Tuesday;
    }

    public List<String> getTuesday()
    {
        return tuesday;
    }
    public void setWednesday(List<String> Wednesday)
    {
        this.wednesday = Wednesday;
    }

    public List<String> getWednesday()
    {
        return wednesday;
    }
    public void setThursday(List<String> Thursday)
    {
        this.thursday = Thursday;
    }

    public List<String> getThursday()
    {
        return thursday;
    }
    public void setFriday(List<String> Friday)
    {
        this.friday = Friday;
    }

    public List<String> getFriday()
    {
        return friday;
    }
    public void setSaturday(List<String> Saturday)
    {
        this.saturday = Saturday;
    }

    public List<String> getSaturday()
    {
        return saturday;
    }
    public void setSunday(List<String> Sunday)
    {
        this.sunday = Sunday;
    }

    public List<String> getSunday()
    {
        return sunday;
    }
    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    public Long getDeptId()
    {
        return deptId;
    }
    public void setAncestors(String ancestors)
    {
        this.ancestors = ancestors;
    }

    public String getAncestors()
    {
        return ancestors;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("lineId", getLineId())
            .append("startDate", getStartDate())
            .append("endDate", getEndDate())
            .append("priority", getPriority())
            .append("Monday", getMonday())
            .append("Tuesday", getTuesday())
            .append("Wednesday", getWednesday())
            .append("Thursday", getThursday())
            .append("Friday", getFriday())
            .append("Saturday", getSaturday())
            .append("Sunday", getSunday())
            .append("deptId", getDeptId())
            .append("ancestors", getAncestors())
            .toString();
    }
}
